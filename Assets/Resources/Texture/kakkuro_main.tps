<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>3</int>
        <key>texturePackerVersion</key>
        <string>3.4.0</string>
        <key>fileName</key>
        <string>/Users/tomoda/Desktop/PlusCross/pluscross/Assets/Resources/Texture/kakkuro_main.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>premultiplyAlpha</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity</string>
        <key>textureFileName</key>
        <filename>kakkuro_main.png</filename>
        <key>flipPVR</key>
        <false/>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>2</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>0</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>2</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>reduceBorderArtifacts</key>
        <false/>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">POT</enum>
            <key>forceSquared</key>
            <false/>
            <key>forceWordAligned</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>data</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>kakkuro_main.txt</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>cleanTransparentPixels</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>innerPadding</key>
            <uint>0</uint>
            <key>extrude</key>
            <uint>0</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Trim</enum>
            <key>heuristicMask</key>
            <false/>
            <key>pivotPoint</key>
            <enum type="SpriteSettings::PivotPoint">Center</enum>
        </struct>
        <key>fileList</key>
        <array>
            <filename>../../../../PlusCrossData/texture/bg_underbar.png</filename>
            <filename>../../../../PlusCrossData/texture/btn_bgmoff.png</filename>
            <filename>../../../../PlusCrossData/texture/btn_bgmon1.png</filename>
            <filename>../../../../PlusCrossData/texture/btn_bgmon2.png</filename>
            <filename>../../../../PlusCrossData/texture/btn_bgmon3.png</filename>
            <filename>../../../../PlusCrossData/texture/btn_buy.png</filename>
            <filename>../../../../PlusCrossData/texture/btn_cancel.png</filename>
            <filename>../../../../PlusCrossData/texture/btn_check_off.png</filename>
            <filename>../../../../PlusCrossData/texture/btn_check.png</filename>
            <filename>../../../../PlusCrossData/texture/btn_del.png</filename>
            <filename>../../../../PlusCrossData/texture/btn_delete.png</filename>
            <filename>../../../../PlusCrossData/texture/btn_exit.png</filename>
            <filename>../../../../PlusCrossData/texture/btn_help.png</filename>
            <filename>../../../../PlusCrossData/texture/btn_hint.png</filename>
            <filename>../../../../PlusCrossData/texture/btn_inputnum.png</filename>
            <filename>../../../../PlusCrossData/texture/btn_language_e.png</filename>
            <filename>../../../../PlusCrossData/texture/btn_language_j.png</filename>
            <filename>../../../../PlusCrossData/texture/btn_left.png</filename>
            <filename>../../../../PlusCrossData/texture/btn_new_e.png</filename>
            <filename>../../../../PlusCrossData/texture/btn_new_j.png</filename>
            <filename>../../../../PlusCrossData/texture/btn_ok.png</filename>
            <filename>../../../../PlusCrossData/texture/btn_play_e.png</filename>
            <filename>../../../../PlusCrossData/texture/btn_play_j.png</filename>
            <filename>../../../../PlusCrossData/texture/btn_restore_e.png</filename>
            <filename>../../../../PlusCrossData/texture/btn_restore_j.png</filename>
            <filename>../../../../PlusCrossData/texture/btn_resume_e.png</filename>
            <filename>../../../../PlusCrossData/texture/btn_resume_j.png</filename>
            <filename>../../../../PlusCrossData/texture/btn_right.png</filename>
            <filename>../../../../PlusCrossData/texture/btn_surrender.png</filename>
            <filename>../../../../PlusCrossData/texture/info_num_00.png</filename>
            <filename>../../../../PlusCrossData/texture/info_num_00w.png</filename>
            <filename>../../../../PlusCrossData/texture/info_num_01.png</filename>
            <filename>../../../../PlusCrossData/texture/info_num_01w.png</filename>
            <filename>../../../../PlusCrossData/texture/info_num_02.png</filename>
            <filename>../../../../PlusCrossData/texture/info_num_02w.png</filename>
            <filename>../../../../PlusCrossData/texture/info_num_03.png</filename>
            <filename>../../../../PlusCrossData/texture/info_num_03w.png</filename>
            <filename>../../../../PlusCrossData/texture/info_num_04.png</filename>
            <filename>../../../../PlusCrossData/texture/info_num_04w.png</filename>
            <filename>../../../../PlusCrossData/texture/info_num_05.png</filename>
            <filename>../../../../PlusCrossData/texture/info_num_05w.png</filename>
            <filename>../../../../PlusCrossData/texture/info_num_06.png</filename>
            <filename>../../../../PlusCrossData/texture/info_num_06w.png</filename>
            <filename>../../../../PlusCrossData/texture/info_num_07.png</filename>
            <filename>../../../../PlusCrossData/texture/info_num_07w.png</filename>
            <filename>../../../../PlusCrossData/texture/info_num_08.png</filename>
            <filename>../../../../PlusCrossData/texture/info_num_08w.png</filename>
            <filename>../../../../PlusCrossData/texture/info_num_09.png</filename>
            <filename>../../../../PlusCrossData/texture/info_num_09w.png</filename>
            <filename>../../../../PlusCrossData/texture/info_num_11.png</filename>
            <filename>../../../../PlusCrossData/texture/input_num_01.png</filename>
            <filename>../../../../PlusCrossData/texture/input_num_02.png</filename>
            <filename>../../../../PlusCrossData/texture/input_num_03.png</filename>
            <filename>../../../../PlusCrossData/texture/input_num_04.png</filename>
            <filename>../../../../PlusCrossData/texture/input_num_05.png</filename>
            <filename>../../../../PlusCrossData/texture/input_num_06.png</filename>
            <filename>../../../../PlusCrossData/texture/input_num_07.png</filename>
            <filename>../../../../PlusCrossData/texture/input_num_08.png</filename>
            <filename>../../../../PlusCrossData/texture/input_num_09.png</filename>
            <filename>../../../../PlusCrossData/texture/main_num_01.png</filename>
            <filename>../../../../PlusCrossData/texture/main_num_02.png</filename>
            <filename>../../../../PlusCrossData/texture/main_num_03.png</filename>
            <filename>../../../../PlusCrossData/texture/main_num_04.png</filename>
            <filename>../../../../PlusCrossData/texture/main_num_05.png</filename>
            <filename>../../../../PlusCrossData/texture/main_num_06.png</filename>
            <filename>../../../../PlusCrossData/texture/main_num_07.png</filename>
            <filename>../../../../PlusCrossData/texture/main_num_08.png</filename>
            <filename>../../../../PlusCrossData/texture/main_num_09.png</filename>
            <filename>../../../../PlusCrossData/texture/main_num_10.png</filename>
            <filename>../../../../PlusCrossData/texture/main_num_11.png</filename>
            <filename>../../../../PlusCrossData/texture/main_num_12.png</filename>
            <filename>../../../../PlusCrossData/texture/sgn_batu.png</filename>
            <filename>../../../../PlusCrossData/texture/sgn_btn_answer_e.png</filename>
            <filename>../../../../PlusCrossData/texture/sgn_btn_answer_j.png</filename>
            <filename>../../../../PlusCrossData/texture/sgn_clearcount_e.png</filename>
            <filename>../../../../PlusCrossData/texture/sgn_clearcount_j.png</filename>
            <filename>../../../../PlusCrossData/texture/sgn_copyright.png</filename>
            <filename>../../../../PlusCrossData/texture/sgn_count_e.png</filename>
            <filename>../../../../PlusCrossData/texture/sgn_count_j.png</filename>
            <filename>../../../../PlusCrossData/texture/sgn_hm_e.png</filename>
            <filename>../../../../PlusCrossData/texture/sgn_hm_j.png</filename>
            <filename>../../../../PlusCrossData/texture/sgn_hmlogo.png</filename>
            <filename>../../../../PlusCrossData/texture/sgn_lamp0.png</filename>
            <filename>../../../../PlusCrossData/texture/sgn_lamp1.png</filename>
            <filename>../../../../PlusCrossData/texture/sgn_lamp2a.png</filename>
            <filename>../../../../PlusCrossData/texture/sgn_lamp2b.png</filename>
            <filename>../../../../PlusCrossData/texture/sgn_lv1.png</filename>
            <filename>../../../../PlusCrossData/texture/sgn_lv2.png</filename>
            <filename>../../../../PlusCrossData/texture/sgn_lv3.png</filename>
            <filename>../../../../PlusCrossData/texture/sgn_lv4.png</filename>
            <filename>../../../../PlusCrossData/texture/sgn_lv5.png</filename>
            <filename>../../../../PlusCrossData/texture/sgn_lvstar.png</filename>
            <filename>../../../../PlusCrossData/texture/sgn_maru1.png</filename>
            <filename>../../../../PlusCrossData/texture/sgn_maru2.png</filename>
            <filename>../../../../PlusCrossData/texture/sgn_maru3.png</filename>
            <filename>../../../../PlusCrossData/texture/sgn_maze.png</filename>
            <filename>../../../../PlusCrossData/texture/sgn_maze1.png</filename>
            <filename>../../../../PlusCrossData/texture/sgn_maze2.png</filename>
            <filename>../../../../PlusCrossData/texture/sgn_maze3.png</filename>
            <filename>../../../../PlusCrossData/texture/sgn_maze4.png</filename>
            <filename>../../../../PlusCrossData/texture/sgn_maze5.png</filename>
            <filename>../../../../PlusCrossData/texture/sgn_plate_clear.png</filename>
            <filename>../../../../PlusCrossData/texture/sgn_plate_hint.png</filename>
            <filename>../../../../PlusCrossData/texture/sgn_plate_input.png</filename>
            <filename>../../../../PlusCrossData/texture/sgn_plate_normal.png</filename>
            <filename>../../../../PlusCrossData/texture/sgn_plate_unclear.png</filename>
            <filename>../../../../PlusCrossData/texture/sgn_select_e.png</filename>
            <filename>../../../../PlusCrossData/texture/sgn_select_j.png</filename>
            <filename>../../../../PlusCrossData/texture/sgn_surrender_e.png</filename>
            <filename>../../../../PlusCrossData/texture/sgn_surrender_j.png</filename>
            <filename>../../../../PlusCrossData/texture/sgn_time.png</filename>
            <filename>../../../../PlusCrossData/texture/sgn_titlelogo_e.png</filename>
            <filename>../../../../PlusCrossData/texture/sgn_titlelogo_j.png</filename>
            <filename>../../../../PlusCrossData/texture/wnd_restart_e.png</filename>
            <filename>../../../../PlusCrossData/texture/wnd_restart_j.png</filename>
            <filename>../../../../PlusCrossData/texture/sgn_create_e.png</filename>
            <filename>../../../../PlusCrossData/texture/sgn_create_j.png</filename>
            <filename>../../../../PlusCrossData/texture/sgn_sum_l.png</filename>
            <filename>../../../../PlusCrossData/texture/sgn_sum_r.png</filename>
            <filename>../../../../PlusCrossData/texture/sgn_plate_mask.png</filename>
            <filename>../../../../PlusCrossData/texture/sgn_sum_off.png</filename>
            <filename>../../../../../pluscros_texture/toast_e.png</filename>
            <filename>../../../../../pluscros_texture/toast_j.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
    </struct>
</data>
