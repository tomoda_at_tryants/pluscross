{"frames": {

"help1_e.png":
{
	"frame": {"x":620,"y":2,"w":624,"h":698},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":11,"y":12,"w":624,"h":698},
	"sourceSize": {"w":640,"h":720}
},
"list1_e.png":
{
	"frame": {"x":2,"y":2,"w":616,"h":702},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":12,"y":13,"w":616,"h":702},
	"sourceSize": {"w":640,"h":720}
},
"list2_e.png":
{
	"frame": {"x":2,"y":706,"w":616,"h":702},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":12,"y":13,"w":616,"h":702},
	"sourceSize": {"w":640,"h":720}
},
"list3_e.png":
{
	"frame": {"x":1246,"y":2,"w":616,"h":702},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":12,"y":13,"w":616,"h":702},
	"sourceSize": {"w":640,"h":720}
},
"list4_e.png":
{
	"frame": {"x":620,"y":702,"w":616,"h":702},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":12,"y":13,"w":616,"h":702},
	"sourceSize": {"w":640,"h":720}
}},
"meta": {
	"app": "http://www.codeandweb.com/texturepacker",
	"version": "1.0",
	"image": "kakkuro_help_e.png",
	"format": "RGBA8888",
	"size": {"w":2048,"h":2048},
	"scale": "1",
	"smartupdate": "$TexturePacker:SmartUpdate:bd4ebffc4b25119abdeafc73908838ad:b2fc118893e90927f4a64431983c8a9a:49946289b6c9be792ee6974453e0f418$"
}
}
