{"frames": {

"helpbg.png":
{
	"frame": {"x":2,"y":2,"w":640,"h":1136},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":640,"h":1136},
	"sourceSize": {"w":640,"h":1136}
},
"mainbg.png":
{
	"frame": {"x":644,"y":2,"w":640,"h":1136},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":640,"h":1136},
	"sourceSize": {"w":640,"h":1136}
}},
"meta": {
	"app": "http://www.codeandweb.com/texturepacker",
	"version": "1.0",
	"image": "kakkuro_bg.png",
	"format": "RGBA8888",
	"size": {"w":2048,"h":2048},
	"scale": "1",
	"smartupdate": "$TexturePacker:SmartUpdate:ee6e284fa37a422622a309ef53853d36:362dd9fd465b6272f611bf0c0ef905a0:7476970f04d0397c118353cb75c71437$"
}
}
