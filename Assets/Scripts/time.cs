﻿using UnityEngine;
using System.Collections;

public class time : MonoBehaviour {

	private static	float	fTime;
	private static	bool	fTimerStart;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
		Timer ();

	}

	public static float		getTime(){
		return	fTime;
	}
	public static void		setTime(float fValue){
		if (fValue < 0.0f)	return;
		fTime = fValue;
	}

	public static	bool	getTimerStartFlag(){
		return	fTimerStart;
	}
	public static	void	setTimerStartFlag(bool flag){
		fTimerStart = flag;
	}

	public static	void	savePref_time(float fValue){
		if (fValue < 0.0f)	return;
		PlayerPrefs.SetFloat ("time", fValue);
	}
	public static	void	loadPref_time(){

		fTime = PlayerPrefs.GetFloat ("time");
	}

	public void	Timer(){
		
		int	nMinute = 0;
		int nSec 	= 0;
		if(fTimerStart == true){
			
			fTime += Time.deltaTime;
			
			nMinute = (int)fTime / 60;
			nSec 	= (int)fTime % 60;
			Transform	timer = GameObject.Find ("UI Root/Panel/time").transform;
			int nSec00 = nSec % 10;
			int nSec01 = nSec / 10;
			
			timer.FindChild ("number_sec00").GetComponent<UISprite> ().spriteName = "info_num_0" + nSec00.ToString ();
			timer.FindChild ("number_sec01").GetComponent<UISprite> ().spriteName = "info_num_0" + nSec01.ToString ();
			
			int nMinute00 = nMinute % 10;
			int nMinute01 = nMinute / 10;
			
			timer.FindChild ("number_minute00").GetComponent<UISprite> ().spriteName = "info_num_0" + nMinute00.ToString ();
			timer.FindChild ("number_minute01").GetComponent<UISprite> ().spriteName = "info_num_0" + nMinute01.ToString ();
			
		}
		
	}

}
