﻿using UnityEngine;
using System.Collections;

public class android_quit : MonoBehaviour {

	private static int	nFrameCount = 0;
	private static int 	nTouchCount = 0;
	private static int 	nFrameMax 	= 180;
	private static bool	fToastShow 	= false;
	private static bool	fTouch 		= false;
	private static bool	fQuit 		= false;

	public static	void	quit(){

		if(fQuit == true){
			if(GoogleMobileAdsDemoScript.getEndFlag() == true){
				
				GoogleMobileAdsDemoScript.setEndFlag(false);
				saveLoad.saveStageData ();
				Application.Quit ();
				
			}
		}
		
		if( fTouch == true ){
			nFrameCount++;
			if( nFrameCount >= nFrameMax ){
				
				nTouchCount = 0;
				nFrameCount = 0;
				fTouch 		= false;
				fToastShow 	= false;
				Destroy(GameObject.Find ("Panel").transform.FindChild ("toast").gameObject);
				
			}
			
		}
		
		if( Application.platform == RuntimePlatform.Android ){
			if( Input.GetKeyDown ( KeyCode.Escape ) ){
				if ( nTouchCount == 0 )	fTouch = true;
				
				nTouchCount++;

				if(fTouch == true && nTouchCount == 1){
					if(fToastShow == false){

						int		nLanguageType 	= gameInfo.getLanguageType ();
						string	spriteName 		= "";
						if(nLanguageType == (int)LANGUAGE_TYPE.JAPANESE){
							spriteName = "toast_j";
						} else if(nLanguageType == (int)LANGUAGE_TYPE.ENGLISH) {
							spriteName = "toast_e";
						}

						//	トースト削除
						fToastShow = true;
						GameObject	toastPrefab = Instantiate (Resources.Load ("Prefabs/toastPrefab"), new Vector3(0.0f, -300.0f, 0.0f), Quaternion.identity) as GameObject;
						toastPrefab.transform.parent = GameObject.Find ("Panel").transform;
						toastPrefab.transform.localPosition = new Vector3 (0.0f, -350.0f, 0.0f);
						toastPrefab.transform.localScale = new Vector3 (1.0f, 1.0f, 1.0f);
						toastPrefab.GetComponent<UISprite>().spriteName = spriteName;
						toastPrefab.name = "toast";
						toastPrefab.SetActive (true);

						//	copyright
						float	fManualHeight 	= 0.0f;
						float 	ap 				= Screen.height * 1.0f / Screen.width * 1.0f;
						fManualHeight 			= (int)( 640.0f * ap );
						float	fSize 			= (fManualHeight -960.0f)/2;
						GameObject.Find ("UI Root/Panel/sgn_copyright").transform.localPosition = new Vector3 (GameObject.Find ("UI Root/Panel/toast").transform.localPosition.x, 
						                                                                                       GameObject.Find ("UI Root/Panel/toast").transform.localPosition.y-fSize,
						                                                                                       GameObject.Find ("UI Root/Panel/toast").transform.localPosition.z);
					}
				}
				
				if( fTouch == true && nTouchCount == 2 ){
					fQuit = true;
					if(gameInfo.getShowInterstitialFlag() == false){
						gameInfo.setShowInterstitialFlag (true);
					}
				}
				
			}
		}
		
	}
	
}
