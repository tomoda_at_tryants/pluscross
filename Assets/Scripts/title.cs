﻿using UnityEngine;
using System.Collections;

public class title : MonoBehaviour {

	public 			int		nSplashCount;
	public static 	bool	fStart;
	public static 	bool	fInit;

	public int	nStartUpCount;
	public int	nShowCount;
	public static bool	fStartUp;


	// Use this for initialization
	void Start () {

		nShowCount = 5;
		if(fStartUp == false){
			fStartUp = true;
			nStartUpCount = PlayerPrefs.GetInt ("StartUpCount_title");
			nStartUpCount++;
			PlayerPrefs.SetInt ("StartUpCount_title", nStartUpCount);
			Debug.Log ("Count:"+nStartUpCount);				

			if(nStartUpCount >= nShowCount){
				if(gameInfo.getShowInterstitialFlag() == false){
					PlayerPrefs.SetInt ("StartUpCount_title", 0);
					gameInfo.setShowInterstitialFlag (true);
				}
			}
		}
		
		if(fStart == true){
			if(fInit == true){

				title_Init ();
				gameInfo.objName_init ();
				
				audioManager	audioData = GameObject.Find ("audioManager").GetComponent<audioManager>();
				audioData.createAudio ((int)AUDIO_TYPE.BGM, (int)AUDIO_BGM.TITLE);
				
				//	FadeOut
				GameObject fadeData = GameObject.Find ("Panel").transform.FindChild ("fadeMask").gameObject;
				fadeData.SetActive (true);
				fadeData.GetComponent<fade> ().setFadeData (1, (int)FADE_TYPE.FADEOUT);
				fade.fFade = true;
				
				Destroy(GameObject.Find ("Panel").transform.FindChild ("touchControl").gameObject);
				Destroy (GameObject.Find ("Panel").transform.FindChild("splashBG").gameObject);
				Destroy (GameObject.Find ("Panel").transform.FindChild ("sgn_hmlogo").gameObject);

			}
		}

	}

	// Update is called once per frame
	void Update () {

		if(fStart == false){

			nSplashCount++;
			if(nSplashCount == 30){
				GameObject.Find ("audioManager").GetComponent<audioManager>().createAudio ((int)AUDIO_TYPE.SE, (int)AUDIO_SE.HMLOGO);
			}
			if(nSplashCount == 200){
				Destroy(GameObject.Find ("Panel").transform.FindChild ("splashBG").gameObject);
				Destroy(GameObject.Find ("Panel").transform.FindChild ("sgn_hmlogo").gameObject);
				fStart = true;
			}

		}

		if(fStart == true){
			if(fInit == false){

				fInit = true;

				title_Init ();
				gameInfo.objName_init ();
				
				audioManager	audioData = GameObject.Find ("audioManager").GetComponent<audioManager>();
				audioData.createAudio ((int)AUDIO_TYPE.BGM, (int)AUDIO_BGM.TITLE);
				
				//	FadeOut
				GameObject fadeData = GameObject.Find ("Panel").transform.FindChild ("fadeMask").gameObject;
				fadeData.SetActive (true);
				fadeData.GetComponent<fade> ().setFadeData (1, (int)FADE_TYPE.FADEOUT);
				fade.fFade = true;

				Destroy(GameObject.Find ("Panel").transform.FindChild ("touchControl").gameObject);

			}
		}

	}

	public void	title_Init(){

//		GameObject.Find ("touchEvent").GetComponent<touchEvent> ().setScene ((int)SCENE.TITLE);
		gameInfo.setSceneNum ((int)SCENE.TITLE);

		int nLanguageType = gameInfo.getLanguageType ();
		string[][]	spriteName_play = new string[][]{

			new string[]{
				"btn_play_j", "btn_play_e"
			},
			new string[]{
				"btn_new_j", "btn_new_e"
			},
			new string[]{
				"btn_resume_j", "btn_resume_e"

			}

		};
		string[] objName = new string[]{
			"btn_play", "btn_start", "btn_continue"
		};
	
		for(int i = 0 ; i < 3 ; i++){

			GameObject.Find ("Panel").transform.FindChild (objName[i]).GetChild (0).GetComponent<UISprite>().spriteName = spriteName_play[i][nLanguageType];
			GameObject.Find ("Panel").transform.FindChild (objName[i]).GetComponent<UIButton>().normalSprite 			= spriteName_play[i][nLanguageType];

		}

		string[] spriteName = new string[]{
			"sgn_titlelogo_j", "sgn_titlelogo_e"
		};
		GameObject.Find ("Panel").transform.FindChild ("sgn_titlelogo").GetComponent<UISprite>().spriteName = spriteName[nLanguageType];

		spriteName = new string[]{
			"btn_language_j", "btn_language_e"
		};
		GameObject.Find ("Panel").transform.FindChild ("btn_language").GetChild (0).GetComponent<UISprite>().spriteName = spriteName[nLanguageType];
		GameObject.Find ("Panel").transform.FindChild ("btn_language").GetComponent<UIButton>().normalSprite 			= spriteName[nLanguageType];

		spriteName = new string[]{
			"sgn_hm_j", "sgn_hm_e"
		};
		GameObject.Find ("Panel").transform.FindChild ("btn_hm").FindChild ("Background").GetComponent<UISprite>().spriteName = spriteName[nLanguageType];

		//	copyright
		float	fManualHeight 	= 0.0f;
		float 	ap 				= Screen.height * 1.0f / Screen.width * 1.0f;
		fManualHeight 			= (int)( 640.0f * ap );
		float	fSize 			= (fManualHeight -960.0f)/2;
		GameObject.Find ("UI Root/Panel/sgn_copyright").transform.localPosition = new Vector3 (GameObject.Find ("UI Root/Panel/sgn_copyright").transform.localPosition.x, 
		                                                                                       GameObject.Find ("UI Root/Panel/sgn_copyright").transform.localPosition.y-fSize,
		                                                                                       GameObject.Find ("UI Root/Panel/sgn_copyright").transform.localPosition.z);

	}

}
