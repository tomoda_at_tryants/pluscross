﻿using UnityEngine;
using System.Collections;

public class saveLoad : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	}

	public static	void	deleteKey(){

		main	mainData = GameObject.Find ("main").GetComponent<main>();

		for(int i = 0 ; i < mainData.nPlateMax ; i++){
			if(PlayerPrefs.HasKey("Number_Input"+i.ToString()))		PlayerPrefs.DeleteKey ("Number_Input"+i.ToString());
			if(PlayerPrefs.HasKey("Number_Answer"+i.ToString()))	PlayerPrefs.DeleteKey ("Number_Answer"+i.ToString ());
			if(PlayerPrefs.HasKey("rowNumber"+i.ToString()))		PlayerPrefs.DeleteKey ("rowNumber"+i.ToString ());
			if(PlayerPrefs.HasKey("colNumber"+i.ToString()))		PlayerPrefs.DeleteKey ("colNumber"+i.ToString ());
			if(PlayerPrefs.HasKey("frameType"+i.ToString()))		PlayerPrefs.DeleteKey("frameType"+i.ToString());
			if(PlayerPrefs.HasKey("UseNumberFlag"+i.ToString ()))	PlayerPrefs.DeleteKey ("UseNumberFlag"+i.ToString ());
		}

	}

	public static	void	saveStageData(){

		if(gameInfo.getSceneNum () == (int)SCENE.MAIN){

			//	PlayerPrefsのキーを削除
			deleteKey();

			main	mainData = GameObject.Find ("main").GetComponent<main>();
			//	レベル保存
			gameInfo.savePref_level (gameInfo.getLevel());
			//	問題タイプ保存
			gameInfo.savePref_stageType (gameInfo.getStageType());
			//	不正解カウンタ保存
			gameInfo.savePref_inCorrectCount ();
			//	コンティニューフラグ
			gameInfo.savePref_continueFlag (true);
			//	経過時間
			time.savePref_time (time.getTime ());
			for(int i = 0 ; i < mainData.nPlateMax ; i++){
				//	入力数字保存
				if(mainData.plateData[i].nNumber_Input > 0){
					PlayerPrefs.SetInt ("Number_Input"+i.ToString(), mainData.plateData[i].nNumber_Input);
				}
				//	答え保存
				if(mainData.plateData[i].nNumber_Answer > 0){
					PlayerPrefs.SetInt ("Number_Answer"+i.ToString (), mainData.plateData[i].nNumber_Answer);
				}
				//	行数値保存
				if(mainData.plateData[i].nRowNumber > 0){
					PlayerPrefs.SetInt ("rowNumber"+i.ToString (), mainData.plateData[i].nRowNumber);
				}
				//	列数値保存
				if(mainData.plateData[i].nColNumber > 0){
					PlayerPrefs.SetInt ("colNumber"+i.ToString (), mainData.plateData[i].nColNumber);
				}
				//	フレームタイプ保存
				if(mainData.plateData[i].nPlateType == (int)PLATE_TYPE.INPUT){

					int	nFrameType = GameObject.Find ("Panel").transform.FindChild ("plates").GetChild (i).FindChild ("frame").GetComponent<frame>().nFrameType;
					if(nFrameType != (int)FRAME_TYPE.NONE){
						PlayerPrefs.SetInt("frameType"+i.ToString (), nFrameType);
					}
				}
				//	使用可能フラグ保存
				PlayerPrefs.SetInt ("UseNumberFlag"+i.ToString (), mainData.plateData[i].nUseNumberFlag_op);
			}

			PlayerPrefs.Save ();
		}
		
	}
	
	public void		loadStageData(){
		
		if(gameInfo.getSceneNum () == (int)SCENE.MAIN){
			
			main	mainData = GameObject.Find ("main").GetComponent<main>();

			gameInfo.setLevel (gameInfo.loadPref_level ());
			gameInfo.setStageType (gameInfo.loadPref_stageType());
			gameInfo.loadPref_inCorrectCount ();
			time.loadPref_time ();

			for(int i = 0 ; i < mainData.nPlateMax ; i++){
				if(PlayerPrefs.HasKey ("Number_Input"+i.ToString())){
					mainData.plateData[i].nNumber_Input 	= PlayerPrefs.GetInt ("Number_Input"+i.ToString ());
				}
				if(PlayerPrefs.HasKey ("Number_Answer"+i.ToString ())){
					mainData.plateData[i].nNumber_Answer 	= PlayerPrefs.GetInt ("Number_Answer"+i.ToString ());
				}
				if(PlayerPrefs.HasKey ("rowNumber"+i.ToString ())){
					mainData.plateData[i].nRowNumber 		= PlayerPrefs.GetInt ("rowNumber"+i.ToString ());
				}
				if(PlayerPrefs.HasKey ("colNumber"+i.ToString())){
					mainData.plateData[i].nColNumber 		= PlayerPrefs.GetInt ("colNumber"+i.ToString());
				}
				if(PlayerPrefs.HasKey ("frameType"+i.ToString ())){
					GameObject.Find ("Panel").transform.FindChild ("plates").GetChild (i).FindChild ("frame").GetComponent<frame>().nFrameType = PlayerPrefs.GetInt ("frameType"+i.ToString ());
				}
				mainData.plateData[i].nUseNumberFlag 		= PlayerPrefs.GetInt ("UseNumberFlag"+i.ToString ());
			}
			
		}
		
	}

}
