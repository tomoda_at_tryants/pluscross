﻿using UnityEngine;
using System.Collections;

public enum FADE_TYPE
{
	FADEIN, FADEOUT
}

public class fade : MonoBehaviour {

	public			int		nFadeTime;
	public			int		nFadeCount;
	public			int		nFadeType;
	public 			float	oneFrameAlphaValue;
	public static	bool	fFade;
	public bool	fCreate;


	// Use this for initialization
	void Start () {

		if(fCreate == false){

			fCreate = true;
			DontDestroyOnLoad (this);

		}
	
	}

	public void	setFadeData(int nFadeTime, int nFadeType){

		this.nFadeTime = (nFadeTime*60);
		this.nFadeType = nFadeType;
		if(nFadeType == (int)FADE_TYPE.FADEIN){

			transform.GetComponent<UISprite>().alpha = 0.0f;

		} else if(nFadeType == (int)FADE_TYPE.FADEOUT){

			transform.GetComponent<UISprite>().alpha = 1.0f;

		}

	}

	void	FixedUpdate(){

		if(fFade == true){
			if(nFadeType == (int)FADE_TYPE.FADEIN){
				
				fadeIn ();
				
			} else if(nFadeType == (int)FADE_TYPE.FADEOUT){
				
				fadeOut ();
				
			}
		}


	}
	
	// Update is called once per frame
	void Update () {
	
	/*	if(fFade == true){
			if(nFadeType == (int)FADE_TYPE.FADEIN){

				fadeIn ();

			} else if(nFadeType == (int)FADE_TYPE.FADEOUT){

				fadeOut ();

			}
		}
	*/
	}

	public	void	fadeIn(){

		nFadeCount++;
		if(nFadeTime > nFadeCount){

			oneFrameAlphaValue = (255.0f/nFadeTime)/255.0f;
			gameObject.GetComponent<UISprite>().alpha+=oneFrameAlphaValue;

		} else {

			gameObject.GetComponent<UISprite>().alpha = 255.0f;
			nFadeTime = 0;
			fFade = false;
			nFadeCount = 0;

		}

	}

	public void		fadeOut(){

		nFadeCount++;
		if(nFadeTime > nFadeCount){
			
			oneFrameAlphaValue = (255.0f/nFadeTime)/255.0f;
			gameObject.GetComponent<UISprite>().alpha-=oneFrameAlphaValue;
			
		} else {

			gameObject.GetComponent<UISprite>().alpha = 0.0f;
			nFadeTime = 0;
			fFade = false;
			nFadeCount = 0;

		}


	}


}
