﻿using UnityEngine;
using System.Collections;

public class stageSelect : MonoBehaviour {
	
	public GameObject	fadePrefab;

	// Use this for initialization
	void Start () {
	
		stageSelect_Init ();

		audioManager	audioData = GameObject.Find ("audioManager").GetComponent<audioManager>();
		audioData.createAudio ((int)AUDIO_TYPE.BGM, (int)AUDIO_BGM.SELECT);

		//	FadeIn
		GameObject fadeData = GameObject.Find ("Panel").transform.FindChild ("fadeMask").gameObject;
		fadeData.SetActive (true);
		fadeData.GetComponent<fade> ().setFadeData (1, (int)FADE_TYPE.FADEOUT);
		fade.fFade = true;

	}

	public void	stageSelect_Init(){

		//	シーン番号設定
		gameInfo.setSceneNum ((int)SCENE.STAGE_SELECT);

		//	言語番号取得
		int nLanguageType = gameInfo.getLanguageType ();

		string[]	spriteName = new string[]{
			"sgn_select_j", "sgn_select_e"
		};
		GameObject.Find ("UI Root/Panel/sgn_revelSelect").GetComponent<UISprite>().spriteName = spriteName[nLanguageType];

		string[]	sgn_clearCountName = new string[]{
			"sgn_clearcount_j", "sgn_clearcount_e"
		};
		string[]	sgn_timesName = new string[]{
			"sgn_count_j", "sgn_count_e"
		};
		string[]	btn_play = new string[]{
			"btn_play_j", "btn_play_e"
		};
		for(int i = 0; i < 5 ; i++){

			string	prefsName 	= "clearCount_lv"+(i+1).ToString ();
			string	objName 	= "stageSelect_lv"+(i+1).ToString ();
			int		nCount 		= PlayerPrefs.GetInt (prefsName);

			string	countStr 	= nCount.ToString ();
			int		nRank 		= nCount.ToString().Length;
			string	findName 	= "";

			findName = "UI Root/Panel/"+objName+"/sgn_clearCount";
			GameObject.Find (findName).GetComponent<UISprite>().spriteName = sgn_clearCountName[nLanguageType];
			findName = "UI Root/Panel/"+objName+"/sgn_count";
			GameObject.Find (findName).GetComponent<UISprite>().spriteName = sgn_timesName[nLanguageType];
			findName = "UI Root/Panel/"+objName+"/btn_play/Background";
			GameObject.Find (findName).GetComponent<UISprite>().spriteName = btn_play[nLanguageType];

			if(nRank > 0){

				findName = "UI Root/Panel/"+objName+"/clearCountNumber";

				for(int j = 0 ; j < 4 ; j ++){
					if(j < nRank){
					
						GameObject.Find ("Panel").transform.FindChild (objName).FindChild ("clearCountNumber").GetChild (j).gameObject.SetActive (true);
						GameObject.Find (findName).transform.GetChild (j).GetComponent<UISprite>().spriteName = "info_num_0"+countStr[(nRank-1)-j].ToString ();

					} else {

						GameObject.Find ("Panel").transform.FindChild (objName).FindChild ("clearCountNumber").GetChild (j).gameObject.SetActive (false);
						GameObject.Find (findName).transform.GetChild (j).GetComponent<UISprite>().spriteName = "info_num_00";

					}
				}

			}

		}

	}

	// Update is called once per frame
	void Update () {

		stageSelect_Init ();

	}
}
