﻿using UnityEngine;
using System.Collections;

public class panel : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
		panel_Init ();
		SetScreen ();

	}

	public void		panel_Init(){
	
		//	端末の向きを制限する
		Screen.autorotateToPortrait 			= true;
		Screen.autorotateToLandscapeLeft 		= false;
		Screen.autorotateToLandscapeRight 		= false;
		Screen.autorotateToPortraitUpsideDown 	= false;

	}

	public void		SetScreen(){

		UIRoot	Root 		= transform.parent.GetComponent<UIRoot>();
		float 	ap 			= Screen.height * 1.0f / Screen.width * 1.0f;
		Root.manualHeight 	= (int)( 640 * ap );
	
		float 	fPanelHeight 	= 960.0f;
		float 	fOffsetY 		= ( (Root.manualHeight - fPanelHeight ) / 2);
		
		transform.localPosition = new Vector3 ( 0.0f, fOffsetY, 0.0f );


	}


	// Update is called once per frame
	void Update () {

	
	}
	
}
