﻿using UnityEngine;
using System.Collections;

public enum FRAME_TYPE
{

	NONE, UNCLEAR, CLEAR
	
}

public class frame : MonoBehaviour {

	public int	nFrameType;
	public int	nFrameTypeMax;

	// Use this for initialization
	void Start () {

		nFrameType = 0;
		nFrameTypeMax = 3;
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
