﻿using UnityEngine;
using System.Collections;

public enum AUDIO_TYPE
{
	BGM, SE, JGL
}

public enum  AUDIO_BGM{
	GAME1, GAME2, GAME3, SELECT, TITLE
}

public enum  AUDIO_SE{
	CANCEL, OK, DELETE, NEXT, HMLOGO, HMMARK, INPUT, INPUTOPEN, LIGHT_CORRECT, LIGHT_INCORRECT
}

public enum  AUDIO_JGL{
	CHECK, CORRECT, INCORRECT
}

public class audioManager : MonoBehaviour {
	
	//	public	int			nAudioType;
	public	AudioClip[]	audio_BGM;
	public	AudioClip[]	audio_SE;
	public	AudioClip[]	audio_JGL;
	public	string[][]	audioName;
	public 	GameObject	audioPrefab;
	
	// Use this for initialization
	void Start () {
	}

	void	Awake(){

		audioName = new string[][]{
			
			new string[]{
				"bgm_game1", "bgm_game2", "bgm_game3", "bgm_select", "bgm_title"
			},
			new string[]{
				"se_cancel", "se_ok", "se_btn_del", "se_btn_next", "se_hmlogo", "se_hmmark", "se_input", "se_inputopen", "light_correct", "light_incorrect"
			},
			new string[]{
				"jgl_check", "jgl_correct", "jgl_incorrect"
			}
			
		};

	}
	
	public void	createAudio(int nAudioType, int nIndex){
		
		GameObject audioData 			= Instantiate (audioPrefab, Vector3.zero, Quaternion.identity) as GameObject;
		audioData.transform.parent 		= transform;

		audioData.GetComponent<audio> ().setAudioType (nAudioType);
		
		switch(nAudioType){
			
		case	(int)AUDIO_TYPE.BGM:

			audioData.audio.name 	= audioName[(int)AUDIO_TYPE.BGM][nIndex];
			audioData.audio.clip 	= audio_BGM[nIndex];
			audioData.audio.volume 	= 1.0f;
			audioData.audio.loop	= true;
			audioData.audio.Play();
			break;
			
		case	(int)AUDIO_TYPE.SE:
			audioData.audio.name 	= audioName[(int)AUDIO_TYPE.SE][nIndex];
			audioData.audio.clip 	= audio_SE[nIndex];
			audioData.audio.volume 	= 1.0f;
			audioData.audio.loop 	= false;
			audioData.audio.Play();
			break;
			
		case	(int)AUDIO_TYPE.JGL:
			audioData.audio.name 	= audioName[(int)AUDIO_TYPE.JGL][nIndex];
			audioData.audio.clip	= audio_JGL[nIndex];
			audioData.audio.volume 	= 1.0f;
			audioData.audio.loop 	= false;
			audioData.audio.Play();
			break;
			
		default:
			return;
			
		}
		
	}

	public void		audioDestroy(string	name){

		int nMax = transform.childCount;
		for(int i = 0 ; i < nMax; i++){

			Transform	childData = transform.GetChild (i).transform;

			if(childData.name == name)	Destroy (childData.gameObject);
	
		}

	}

	// Update is called once per frame
	void Update () {
		
	}
	
}

