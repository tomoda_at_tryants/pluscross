﻿using UnityEngine;
using System.Collections;

public class touchEvent : MonoBehaviour {

	public int				nCount;
	public int				MethodNum;
	public int				nDeleteButtonCount;
	public int				nBgmType;
	public int				nHelpPage;
	public string			TouchButtonName;
	public bool				fEnd;
	public bool				fTouch_plate_input;
	public bool				fTouch_input_num;
	public bool				fTouch_input_frame;

	public static bool		fInput;
	public RaycastHit2D		hitData;
	public Ray				ray;
	public delegate void	Function();
	public Function[][]		TouchFunctionMethod;
	public string[][]		helpPageName;

	public int				nType;
	public int				nAnimationCount;

	public plate			plateObj_op;

	public string[]			nFrameName;
	public string[]			spriteName_frame;

	public int	nSumType;
	public int	nHintPos;
	public int	nIndex;
	public bool	fBegin;

	public int	nStartUpCount;
	public int	nShowCount;
	public bool	fStartUp;


	public GameObject	maskPrefab;

	// Use this for initialization
	void Start () {
	
		touchEvent_Init ();
	//	Debug.Log ("Path:"+Application.persistentDataPath);

		nShowCount = 5;

	}

	//	バックグラウンドに移行した際の処理
	void	OnApplicationPause(bool pause){

		if(Application.platform == RuntimePlatform.IPhonePlayer){
			//	バックグラウンドに移行
			if(pause == true){
				if(gameInfo.getSceneNum () == (int)SCENE.MAIN){

					Application.runInBackground = true;
					//	ステージデータセーブ
					saveLoad.saveStageData ();
				//	gameInfo.setContinueFlag(true);
				//	gameInfo.savePref_continueFlag (true);
					Application.runInBackground = false;

				} else {

					gameInfo.savePref_continueFlag (false);

				}
			}
		}

	}

	void	FixedUpdate(){

		if (fInput == true)				TouchFunctionMethod [gameInfo.getSceneNum ()][MethodNum] ();
		if (fTouch_plate_input == true)	function_plate_input ();

	}
	
	// Update is called once per frame
	void Update () {

		TouchEvent_Main ();
		android_quit.quit ();
	
	}

	public void		touchEvent_Init(){
	
		gameInfo.setLanguageType (gameInfo.loadPref_languageType());
		nCount 					= 0;
		MethodNum 				= 0;
		nDeleteButtonCount 		= 0;
		TouchButtonName 		= "";
		fTouch_plate_input 		= false;
		fTouch_input_frame = false;
		fTouch_input_num 		= false;
		fInput 					= false;
		fEnd 					= false;
		ray 					= new Ray ();
		hitData 				= new RaycastHit2D ();
		helpPageName 			= new string[][]{

			new string[]{
				"help1_j", "list1_j", "list2_j", "list3_j", "list4_j"
			},
			new string[]{
				"help1_e", "list1_e", "list2_e", "list3_e", "list4_e"
			}

		};
		nFrameName = new string[]{
			"", "sgn_plate_clear", "sgn_plate_unclear"
		};
		spriteName_frame = new string[]{
			"main_num_10", "main_num_11", "main_num_12"
		};
		TouchFunctionMethod 	= new[]{ 

			new Function[]{
		
				new Function(function_play), new Function(function_start), new Function(function_continue), new Function(function_hm), new Function(function_language), 
				new Function(function_help)

			},

			new Function[]{

				new Function(function_help_left), new Function(function_help_right), new Function(function_help_exit)

			},

			new Function[]{

				new Function(function_return), new Function(function_revelAction), new Function(function_revelAction), new Function(function_revelAction), new Function(function_revelAction), 
				new Function(function_revelAction)

			},

			new Function[]{

				new Function(function_hint), new Function(function_check), new Function(function_mainSurrender),
				new Function(function_bgm), new Function(function_surrender_ok), new Function(function_surrender_cancel), new Function(function_hintLeft), new Function(function_hintRight),
				new Function(function_hintExit), new Function(function_restart), new Function(function_restart_ok), new Function(function_restart_cancel), new Function(function_delete),
				new Function(function_plateInput), new Function(function_plateHint), new Function(function_next)

			}
		
		};

	}

	public void		TouchEvent_Main(){

		if (fInput == true)		return;
		if (fade.fFade == true)	return;
		if (gameInfo.getSceneNum () == (int)SCENE.MAIN && gameInfo.getGameStartFlag () == false)	return;

		//	ボタンが押された瞬間
		if(Input.GetMouseButtonDown (0)){
			Vector2		tapPoint 	= GameObject.Find ("Camera").camera.ScreenToWorldPoint (Input.mousePosition);
			Collider2D	collition2D = Physics2D.OverlapPoint (tapPoint);
			if(collition2D){		
				//	指を押した時に発射されるRayに衝突したオブジェクトのデータを取得
				hitData = Physics2D.Raycast (tapPoint, -Vector2.up);
				if(hitData){
					if(hitData.collider.name == "touchControl")	return;
					//	TweenScaleのアニメーション
					if(hitData.collider.tag != "plate"){
						TweenScale.Begin(hitData.collider.gameObject, 0.1f, new Vector3(1.2f, 1.2f, 1.0f));
					} 
					//	数字入力ボタンの描画優先値を設定
					if(hitData.collider.tag == "input_num"){
						hitData.collider.transform.FindChild ("Background").GetComponent<UISprite>().depth = 22;
					}

					audioCheck ();

				}
			}
		}

		if(hitData){
			if(hitData.collider.tag != "plate"){
				if(Input.GetMouseButton(0) == false){

					//	TweenScaleのアニメーション
					TweenScale.Begin(hitData.collider.gameObject, 0.1f, new Vector3(1.0f, 1.0f, 1.0f));
					//	数字入力ボタンの描画優先値を設定	
					if(hitData.collider.tag == "input_num"){
						hitData.collider.transform.FindChild ("Background").GetComponent<UISprite>().depth = 20;
					}
				}
			}
		}

		if( Input.GetMouseButtonUp (0) ){

			Vector2		tapPoint 	= GameObject.Find ("Camera").camera.ScreenToWorldPoint (Input.mousePosition);
			Collider2D	collition2D = Physics2D.OverlapPoint (tapPoint);
			if(collition2D){		
				//	指を離した時に発射するRayに衝突したオブジェクトのデータを取得
				RaycastHit2D	ButtonUphitData = Physics2D.Raycast (tapPoint, -Vector2.up);
				if(hitData.collider == ButtonUphitData.collider){
					if(hitData.collider.name == "touchControl")	return;
				
					//	シーン「main」なら
					if(gameInfo.getSceneNum () == (int)SCENE.MAIN){
						//	ゲームが開始していないなら関数を終了する
						if(gameInfo.getGameStartFlag() == false)	return;
						//	数字入力ボタンの描画優先値を設定
						if(hitData.collider.tag == "input_num"){
							hitData.collider.transform.FindChild ("Background").GetComponent<UISprite>().depth = 20;
							hitData.collider.transform.localScale = new Vector3(1.0f, 1.0f, 1.0f);
						}

						//	表示状態になっている削除ボタンを非表示にする
						int nMax = GameObject.Find ("main").GetComponent<main> ().nPlateMax;
						for(int i = 0 ; i < nMax ; i++){
							plate	plateData = GameObject.Find ("Panel").transform.FindChild ("plates").GetChild (i).GetComponent<plate>();
							if(plateData.plateData.nPlateType == (int)PLATE_TYPE.INPUT){
								if(plateData.transform.FindChild ("btn_delete").gameObject.activeSelf == true){
									plateData.transform.FindChild ("btn_delete").gameObject.SetActive (false);
								}
							}
						}

						//	入力マスにマスクが設定されていたら削除
						for(int i = 0 ; i < nMax ; i++){

							plate	plateData = GameObject.Find ("Panel").transform.FindChild ("plates").GetChild (i).GetComponent<plate>();
							if(plateData.plateData.nPlateType == (int)PLATE_TYPE.INPUT){
								for(int j = 0 ; j < plateData.transform.childCount ; j++){
									if(plateData.transform.GetChild (j).name == "mask"){

										Destroy (plateData.transform.GetChild (j).gameObject);
							
									}
								}
							}
						}

						//	合計表示データを非表示にする
						if(hitData.collider.name != "plate_hint"){
							nMax = GameObject.Find ("Panel").transform.FindChild ("bar_normal").FindChild ("sgn_sum").transform.childCount;
							for(int i = 0 ; i < nMax ; i++){
								GameObject.Find ("Panel").transform.FindChild("bar_normal").FindChild("sgn_sum").GetChild (i).gameObject.SetActive (false);
							}
							GameObject.Find ("Panel").transform.FindChild ("bar_normal").FindChild ("sgn_sum").GetComponent<UISprite>().spriteName = "sgn_sum_off";
						}

						//	全マスのタッチフラグを下げる
						touchEvent_TouchFlagOff();
		
					}

					if(hitData.collider.name == "plate_normal" ){
	
						fTouch_plate_input 	= false;
						fTouch_input_num 	= false;
						GameObject.Find ("Panel").transform.FindChild ("bar_number").gameObject.SetActive (false);
						GameObject.Find ("Panel").transform.FindChild ("bar_surrender").gameObject.SetActive (false);
						GameObject.Find ("Panel").transform.FindChild ("bar_normal").gameObject.SetActive (true);

					} else if(hitData.collider.tag == "input_num"){ 

						//	フレーム変更ボタンがタッチされたら
						if(hitData.collider.name == "frame"){
							fTouch_input_frame = true;
						}
						fTouch_input_num 	= true;

					} else {
				
						fTouch_plate_input	= false;
						fTouch_input_num 	= false;

						TouchButtonName = hitData.collider.name;
						TouchEvent_SearchButton ();

					}
				}
			}

		}

	}

	public void	function_plateInput(){

		plateObj_op = hitData.collider.GetComponent<plate>();
		fTouch_plate_input = true;
		//	バーの表示設定
		GameObject.Find ("Panel").transform.FindChild ("bar_normal").gameObject.SetActive (false);
		GameObject.Find ("Panel").transform.FindChild ("bar_surrender").gameObject.SetActive (false);
		GameObject.Find ("Panel").transform.FindChild ("bar_number").gameObject.SetActive (true);
		audioManager	audioData 	= GameObject.Find ("audioManager").GetComponent<audioManager> ();
		audioData.createAudio ((int)AUDIO_TYPE.SE, (int)AUDIO_SE.INPUTOPEN);

		//	タッチされたマスを表すマスク生成
		createMask (plateObj_op.transform, new Color((247.0f/255.0f), (206.0f/255.0f), (40.0f/255.0f), ((255.0f*0.8f)/255.0f)));
		
		//	タッチされた入力マスの現在のフレームタイプを取得し、フレームボタンのスプライト画像に反映させる
		int	nFrameType = plateObj_op.transform.FindChild ("frame").GetComponent<frame>().nFrameType;
		GameObject.Find ("Panel").transform.FindChild ("bar_number").FindChild ("frame").FindChild ("Background").GetComponent<UISprite>().spriteName = spriteName_frame[nFrameType];
		
		//	数字が入力されているなら消去ボタン表示
		if(plateObj_op.plateData.fInput == true){
			plateObj_op.transform.FindChild ("btn_delete").gameObject.SetActive (true);
		}

		fInput = false;

	}

	void	createMask( Transform parentData, Color colorData ){

		GameObject	maskData 		= Instantiate (Resources.Load ("Prefabs/maskPrefab") as GameObject, Vector3.zero, Quaternion.identity) as GameObject;
		maskData.transform.parent 	= parentData;
		maskData.GetComponent<UISprite> ().depth = parentData.Find ("Background").GetComponent<UISprite> ().depth + 1;
		maskData.GetComponent<UISprite> ().color = colorData;
		maskData.GetComponent<UISprite> ().depth = 5;
		maskData.transform.localScale = new Vector3 (1.0f, 1.0f, 1.0f);
		maskData.transform.localPosition = new Vector3 (0.0f, 0.0f, 0.0f);
		maskData.name = "mask";
		maskData.SetActive (true);

	}

	public void	function_plateHint(){

		fTouch_plate_input 	= false;
		fTouch_input_frame 	= false;
		fTouch_input_num 	= false;

		//	バーの表示設定
		GameObject.Find ("Panel").transform.FindChild ("bar_normal").gameObject.SetActive (true);
		GameObject.Find ("Panel").transform.FindChild ("bar_surrender").gameObject.SetActive (false);
		GameObject.Find ("Panel").transform.FindChild ("bar_number").gameObject.SetActive (false);

		Vector3[][] hintNumberPos = new Vector3[][]{

			new Vector3[]{
				new Vector3(-10.0f, 37.0f, 0.0f), new Vector3(-21.0f, 37.0f, 0.0f)
			},
			new Vector3[]{
				new Vector3(-31.0f, 13.0f, 0.0f), new Vector3(-42.0f, 13.0f, 0.0f)
			}

		};
		Vector3[][] sumNumberPos = new Vector3[][]{

			new Vector3[]{
				new Vector3(33.0f, 25.0f, 0.0f), new Vector3(11.0f, 25.0f, 0.0f)
			},
			new Vector3[]{
				new Vector3(-15.0f, -22.0f, 0.0f), new Vector3(-37.0f, -22.0f, 0.0f)
			}

		};

		//	タッチしたヒントマスのデータ取得
		plate	hintPlate 	= hitData.collider.GetComponent<plate>();
		int 	nSumTypeMax = 0;
		string 	spriteName 	= "";
		int		nValue 		= 0;

		//	過去にタッチしたヒントマスと現在タッチしているヒントマスが違うなら初期化
		if(nHintPos != hitData.collider.GetComponent<plate>().plateData.nPos){

			//	現在タッチしているヒントマスの番号取得
			nHintPos 	= hitData.collider.GetComponent<plate>().plateData.nPos;
			nSumType 	= 0;
			nIndex 		= 0;
			fBegin 		= true;

		} 

		//	行と列にヒントが設定されている
		if(hintPlate.plateData.nRowCount > 0 && hintPlate.plateData.nColCount > 0){

			int[]		nAreaArray = new int[]{0 ,1 ,2};
			if(fBegin == false){
				nSumTypeMax = 3;
				nSumType++;
				nSumType %= 3;
			} else {
				fBegin 		= false;
				nIndex 		= 0;
				nSumType 	= 0;
			}
			string[]	spriteName_sum = new string[]{ "sgn_sum_r", "sgn_sum_l", "sgn_sum_off" };
			spriteName = spriteName_sum[nSumType];
			nIndex = nAreaArray[nSumType];
			if(nIndex == 0){
				nValue = hintPlate.plateData.nRowNumber;
			} else if(nIndex == 1){
				nValue = hintPlate.plateData.nColNumber;
			} else {
				nValue = 0;
			}

		//	行だけにヒントが設定されている
		} else if(hintPlate.plateData.nRowCount > 0 && hintPlate.plateData.nColCount == 0){
	
			int[]		nAreaArray = new int[]{0, 2};
			if(fBegin == false){
				nSumTypeMax = 2;
				nSumType++;
				nSumType %= 2;
			} else {
				fBegin = false;
				nIndex = 0;
				nSumType = 0;
			}
			string[] spriteName_sum = new string[]{ "sgn_sum_r", "sgn_sum_off" };
			spriteName = spriteName_sum[nSumType];
			nIndex = nAreaArray[nSumType];
			if(nIndex == 0){
				nValue = hintPlate.plateData.nRowNumber;
			} else {
				nValue = 0;
			}

		//	列だけにヒントが設定されている
		} else if(hintPlate.plateData.nRowCount == 0 && hintPlate.plateData.nColCount > 0){

			int[]	nAreaArray = new int[]{1, 2};
			if(fBegin == false){
				nSumTypeMax = 2;
				nSumType++;
				nSumType %= 2;
			} else{
				fBegin = false;
				nIndex = 1;
				nSumType = 0;
			}
			string[] spriteName_sum = new string[]{ "sgn_sum_l", "sgn_sum_off" };
			spriteName = spriteName_sum[nSumType];
			nIndex = nAreaArray[nSumType];
			if(nIndex == 1){
				nValue = hintPlate.plateData.nColNumber;
			} else {
				nValue = 0;
			}

		}

		int nMax = 0;
		if(nIndex == (int)AREA.ROW){
			nMax = hintPlate.plateData.nRowCount;
		} else if(nIndex == (int)AREA.COL){
			nMax = hintPlate.plateData.nColCount;
		} else {
		}
		if(nIndex != 2){
			for(int i = 1 ; i < nMax+1 ; i++){
				int	nInputPos = 0;
				if(nIndex == (int)AREA.ROW){
					nInputPos = nHintPos+i;
				} else if(nIndex == (int)AREA.COL){
					nInputPos = nHintPos+(GameObject.Find ("main").transform.GetComponent<main>().nRowMax*i);
				}
		
				Transform	plateData = GameObject.Find ("Panel").transform.FindChild ("plates").GetChild (nInputPos);
				//	タッチされたマスを表すマスク生成
				createMask (plateData.transform, new Color((249.0f/255.0f), (220.0f/255.0f), (96.0f/255.0f), ((255.0f/2.0f)/255.0f)));
				
			}
		}

		//	//	合計数表示データ取得
		Transform	sgn_sumData = GameObject.Find ("Panel").transform.FindChild ("bar_normal").FindChild ("sgn_sum");
		sgn_sumData.GetComponent<UISprite>().spriteName = spriteName;

		nMax = sgn_sumData.childCount;
		if(nSumType == nSumTypeMax-1){
			//	数値を非表示にする
			for(int i = 0 ; i < nMax ; i++){
				sgn_sumData.GetChild (i).gameObject.SetActive (false);
			}
		} else {
			//	数値を表示する
			for(int i = 0 ; i < nMax ; i++){
				sgn_sumData.GetChild (i).gameObject.SetActive (true);
			}
			//	タッチしたヒントマスが参照している入力マスの合計を取得
			int	nSumValue = GameObject.Find ("main").GetComponent<main>().getInputNumberSum (hintPlate.plateData.nPos, nIndex);
			//	ヒント値が１桁
			if(nValue > 2 && nValue < 10){

				sgn_sumData.FindChild ("hintNumber_01").localPosition = hintNumberPos[nIndex][0];
				sgn_sumData.FindChild ("hintNumber_01").GetComponent<UISprite> ().spriteName = "info_num_0" + nValue+"w";
				sgn_sumData.FindChild ("hintNumber_02").localPosition = hintNumberPos[nIndex][1];
				sgn_sumData.FindChild ("hintNumber_02").gameObject.SetActive (false);

				//	ヒント値が２桁
			} else if(nValue > 9 && nValue < 46){

				int	nRankValue = 0;

				sgn_sumData.FindChild ("hintNumber_01").localPosition = hintNumberPos[nIndex][0];
				nRankValue = nValue % 10;
				sgn_sumData.FindChild ("hintNumber_01").GetComponent<UISprite> ().spriteName = "info_num_0" + nRankValue+"w";
				sgn_sumData.FindChild ("hintNumber_02").localPosition = hintNumberPos[nIndex][1]; 
				nRankValue = nValue / 10;
				sgn_sumData.FindChild ("hintNumber_02").GetComponent<UISprite> ().spriteName = "info_num_0" + nRankValue+"w";

			}

			//	合計数が１桁
			if(nSumValue >= 0 && nSumValue < 10){

				Vector3[]	bigPos = new Vector3[]{

					new Vector3(22.0f, 25.0f, 0.0f),
					new Vector3(-26.0f, -22.0f, 0.0f)

				};

				sgn_sumData.FindChild ("sumNumber_01").localPosition = sumNumberPos[nIndex][0];
				sgn_sumData.FindChild ("sumNumber_01").GetComponent<UISprite> ().spriteName = "info_num_0" + nSumValue;
				sgn_sumData.FindChild ("sumNumber_01").GetComponent<UISprite> ().width 		= 28;
				sgn_sumData.FindChild ("sumNumber_01").GetComponent<UISprite> ().height 	= 40;
				sgn_sumData.FindChild ("sumNumber_01").localPosition = bigPos[nIndex];
				sgn_sumData.FindChild ("sumNumber_02").localPosition = sumNumberPos[nIndex][1];
				sgn_sumData.FindChild ("sumNumber_02").gameObject.SetActive (false);

			//	合計数が２桁
			} else if(nSumValue >= 10){

				int	nRankValue = 0;

				sgn_sumData.FindChild ("sumNumber_01").localPosition = sumNumberPos[nIndex][0];
				nRankValue = nSumValue % 10;
				sgn_sumData.FindChild ("sumNumber_01").GetComponent<UISprite> ().spriteName = "info_num_0" + nRankValue;
				sgn_sumData.FindChild ("sumNumber_01").GetComponent<UISprite> ().width 		= 22;
				sgn_sumData.FindChild ("sumNumber_01").GetComponent<UISprite> ().height 	= 34;
				sgn_sumData.FindChild ("sumNumber_02").localPosition = sumNumberPos[nIndex][1];
				nRankValue = nSumValue / 10;
				sgn_sumData.FindChild ("sumNumber_02").GetComponent<UISprite> ().spriteName = "info_num_0" + nRankValue;

			}

		}

		fInput = false;

	}

	public void	audioCheck(){

		string[][][]	audioManager = new string[][][]{

			//	ok
			new string[][]{
				new string[]{"btn_play", "btn_start", "btn_continue", "btn_language", "btn_help"},
				new string[]{""},
				new string[]{"btn_play"},
				new string[]{"btn_check", "btn_hint", "btn_restart", "btn_surrender_ok", "main_num_01", "main_num_02", "main_num_03", "main_num_04", "main_num_05", 
							 "main_num_06", "main_num_07", "main_num_08", "main_num_09", "frame", "btn_ok", "btn_surrender", "btn_next"}
			},
			//	cancel
			new string[][]{
				new string[]{"btn_quit", "btn_quit_cancel"},
				new string[]{"btn_help_exit"},
				new string[]{"btn_return"},
				new string[]{"btn_surrender_cancel", "btn_cancel", "delete", "btn_exit"}
			},
			//	next
			new string[][]{
				new string[]{""},
				new string[]{"btn_help_left", "btn_help_right"},
				new string[]{""},
				new string[]{"btn_left", "btn_right"},
			},
			//	hmmark
			new string[][]{
				new string[]{"btn_hm"},
				new string[]{""},
				new string[]{""},
				new string[]{""}
			},
			//	input
			new string[][]{
				new string[]{""},
				new string[]{""},
				new string[]{""},
				new string[]{"plate_input", "btn_hint", "btn_normal"}
			},
			//	delete
			new string[][]{
				new string[]{""},
				new string[]{""},
				new string[]{""},
				new string[]{"btn_delete"}
			}
		};
		int[]	nSENumber = new int[]{(int)AUDIO_SE.OK, (int)AUDIO_SE.CANCEL, (int)AUDIO_SE.NEXT, (int)AUDIO_SE.HMMARK, (int)AUDIO_SE.INPUT, (int)AUDIO_SE.DELETE};

		for(int i = 0 ; i < audioManager.Length ; i++){

			int	nMax = audioManager[i][gameInfo.getSceneNum ()].Length;
			audioManager	audioData 	= GameObject.Find ("audioManager").GetComponent<audioManager>();
			for(int j = 0 ; j < nMax ; j++){
				if(hitData.collider.name == audioManager[i][gameInfo.getSceneNum ()][j]){

					audioData.createAudio ((int)AUDIO_TYPE.SE, nSENumber[i]);
					return;

				}
			}
		}

	}

	public void	touchEvent_TouchFlagOff(){

		int	nPlateMax = GameObject.Find ("main").GetComponent<main>().nPlateMax;
		//	全入力マスのタッチフラグを下げる
		for(int i = 0 ; i < nPlateMax ; i++){
			
			GameObject.Find ("main").GetComponent<main>().plateData[i].fTouch = false;
			GameObject.Find ("UI Root/Panel/plates").transform.GetChild (i).GetComponent<plate>().plateData.fTouch = false;
			
		}

	}

	public void		function_plate_input(){
	
		plateObj_op.plateData.fTouch = true;
		GameObject.Find ("main").GetComponent<main> ().plateData [plateObj_op.plateData.nPos].fTouch = true;
	
		if(fTouch_input_num == true){

			hitData.collider.transform.FindChild ("Background").GetComponent<UISprite>().depth = 20;

			//	タッチされたボタンがフレームボタン
			if(hitData.collider.name == "frame"){
				if(fTouch_input_frame == true){

					//	フレームデータの設定
					frame	frameData = GameObject.Find ("Panel").transform.FindChild ("plates").GetChild (plateObj_op.plateData.nPos).FindChild ("frame").GetComponent<frame>();
					frameData.nFrameType++;
					frameData.nFrameType %= frameData.nFrameTypeMax;
					frameData.GetComponent<UISprite>().spriteName = nFrameName[frameData.nFrameType];
					//	スプライト名変更
					GameObject.Find ("Panel").transform.FindChild ("bar_number").FindChild ("frame").FindChild ("Background").GetComponent<UISprite>().spriteName = spriteName_frame[frameData.nFrameType];

					fTouch_input_frame 				= false;
					fTouch_input_num 					= false;

				}
			//	フレームボタン以外
			} else {

				//　入力数字設定
				plateObj_op.plateData.nNumber_Input = hitData.collider.GetComponent<inputnum> ().nNum;
				plateObj_op.plateData.fInput 		= true;
				GameObject.Find ("main").GetComponent<main> ().plateData [plateObj_op.plateData.nPos].nNumber_Input = plateObj_op.plateData.nNumber_Input;
				GameObject.Find ("main").GetComponent<main> ().plateData [plateObj_op.plateData.nPos].fInput 		= true;
				GameObject.Find ("UI Root/Panel/plates").transform.GetChild (plateObj_op.plateData.nPos).FindChild ("number").gameObject.SetActive (true);
				GameObject.Find ("UI Root/Panel/plates").transform.GetChild (plateObj_op.plateData.nPos).FindChild ("number").GetComponent<UISprite>().spriteName = "info_num_0"+plateObj_op.plateData.nNumber_Input.ToString ();
				fTouch_input_num = false;

				//	バーの表示設定
				GameObject.Find ("Panel").transform.FindChild ("bar_number").gameObject.SetActive (false);
				GameObject.Find ("Panel").transform.FindChild ("bar_normal").gameObject.SetActive (true);

			}

		}

	}

	public void		TouchEvent_SearchButton(){

		if (fInput == true)	return;

		int 	LoopMax 	= GameObject.Find ("Panel").transform.childCount;
		for( int i = 0 ; i < LoopMax; i++ ){
			//	タッチ制御するオブジェクトがタッチされたら終了
			if(TouchButtonName == "touchControl")	return;
			if( TouchButtonName == gameInfo.getObjName (i, gameInfo.getSceneNum ()) ){

				MethodNum 	= i;
				if(MethodNum > TouchFunctionMethod[gameInfo.getSceneNum()].Length){
					MethodNum 	= 0;
					fInput 		= false;
					return;
				}
				fInput 		= true;
				
				return;

			}
		}
		
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	Title
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	//	「はじめる」ボタンを押した時の処理
	public void		function_play(){

		bool 	fContinue = false;
		fContinue = gameInfo.loadPref_continueFlag ();

		//	セーブデータがあるならボタンの表示設定
		if(fContinue == true){

			GameObject.Find ("Panel").transform.FindChild ("btn_play").gameObject.SetActive (false);
			GameObject.Find ("Panel").transform.FindChild ("btn_start").gameObject.SetActive (true);
			GameObject.Find ("Panel").transform.FindChild ("btn_continue").gameObject.SetActive (true);
			fInput = false;

		} else {
			if(nCount == 0){

				//	FadeIn
				GameObject fadeData = GameObject.Find ("Panel").transform.FindChild ("fadeMask").gameObject;
				fadeData.SetActive (true);
				fadeData.GetComponent<fade> ().setFadeData (1, (int)FADE_TYPE.FADEIN);
				fade.fFade = true;

			}

			nCount++;
			if(nCount>=60){
				
				nCount = 0;
				gameInfo.setSceneNum ((int)SCENE.STAGE_SELECT);
				fInput = false;
				Application.LoadLevel ("stageSelect");
				
			}

		}
	
	}

	//	「最初から始める」ボタンを押した時の処理
	public void		function_start(){

		if(nCount == 0){

			//	FadeIn
			GameObject fadeData = GameObject.Find ("Panel").transform.FindChild ("fadeMask").gameObject;
			fadeData.SetActive (true);
			fadeData.GetComponent<fade> ().setFadeData (1, (int)FADE_TYPE.FADEIN);
			fade.fFade = true;

		}

		nCount++;

		if(nCount>=60){
			
			nCount = 0;
			gameInfo.setSceneNum ((int)SCENE.STAGE_SELECT);
			gameInfo.setContinueFlag (false);
			gameInfo.savePref_continueFlag (false);
			fInput = false;
			Application.LoadLevel ("stageSelect");
			
		}

	}

	//	「続きから始める」ボタンを押した時の処理
	public void		function_continue(){

		if(nCount == 0){
			
			//	FadeIn
			GameObject fadeData = GameObject.Find ("Panel").transform.FindChild ("fadeMask").gameObject;
			fadeData.SetActive (true);
			fadeData.GetComponent<fade> ().setFadeData (1, (int)FADE_TYPE.FADEIN);
			fade.fFade = true;
			
		}

		nCount++;
		
		if(nCount>=60){
			
			nCount = 0;
			gameInfo.setSceneNum ((int)SCENE.MAIN);
			fInput = false;
			Application.LoadLevel ("main");
			
		}

	}

	//	ロゴボタンを押した時の処理
	public void		function_hm(){

		nCount++;

		if(nCount >= 60){

			nCount = 0;

			if (Application.platform == RuntimePlatform.Android) {
				Application.OpenURL ("https://play.google.com/store/apps/developer?id=Happymeal+Inc.");
			} else if( Application.platform == RuntimePlatform.IPhonePlayer){
				Application.OpenURL ("http://itunes.apple.com/WebObjects/MZSearch.woa/wa/search?entity=software&media=software&submit=seeAllLockups&term=tiddlygame");
			}
			fInput = false;
		}

	}

	//	言語切り替えボタンを押した時の処理
	public void		function_language(){

		string[]	spriteName = new string[]{
			"btn_language_j", "btn_language_e"
		};

		int nLanguageType = gameInfo.getLanguageType ();
		nLanguageType++;
		nLanguageType %= spriteName.Length;

		//	言語を保存し設定
		gameInfo.savePref_languageType (nLanguageType);
		gameInfo.setLanguageType (nLanguageType);

		GameObject.Find ("UI Root/Panel/btn_language/Background").GetComponent<UISprite>().spriteName 	= spriteName[nLanguageType];
		GameObject.Find ("UI Root/Panel/btn_language").GetComponent<UIButton>().normalSprite 			= spriteName[nLanguageType];

		string[][]	spriteName_play = new string[][]{
			
			new string[]{
				"btn_play_j", "btn_play_e"
			},
			new string[]{
				"btn_new_j", "btn_new_e"
			},
			new string[]{
				"btn_resume_j", "btn_resume_e"
					
			}
			
		};
		string[] objName = new string[]{
			"btn_play", "btn_start", "btn_continue"
		};
		
		for(int i = 0 ; i < 3 ; i++){
			
			GameObject.Find ("Panel").transform.FindChild (objName[i]).FindChild ("Background").GetComponent<UISprite>().spriteName = spriteName_play[i][nLanguageType];
			GameObject.Find ("Panel").transform.FindChild (objName[i]).GetComponent<UIButton>().normalSprite 						= spriteName_play[i][nLanguageType];
			
		}

		spriteName = new string[]{
			"sgn_titlelogo_j", "sgn_titlelogo_e"
		};
		GameObject.Find ("Panel").transform.FindChild ("sgn_titlelogo").GetComponent<UISprite>().spriteName = spriteName[nLanguageType];

		spriteName = new string[]{
			"sgn_hm_j", "sgn_hm_e"
		};
		GameObject.Find ("Panel").transform.FindChild ("btn_hm").FindChild ("Background").GetComponent<UISprite>().spriteName 	= spriteName[nLanguageType];
		GameObject.Find ("Panel").transform.FindChild ("btn_hm").GetComponent<UIButton>().normalSprite 							= spriteName[nLanguageType];

		fInput = false;

	}

	//	ヘルプボタンを押した時の処理
	public void		function_help(){

		if(nCount == 0){
			
			//	FadeOut
			GameObject fadeData = GameObject.Find ("Panel").transform.FindChild ("fadeMask").gameObject;
			fadeData.GetComponent<fade> ().setFadeData (1, (int)FADE_TYPE.FADEIN);
			fade.fFade = true;

		}

		nCount++;

		if(nCount >= 60){

			nCount = 0;
			fInput = false;
			Application.LoadLevel ("help");

		}

	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	help
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	ヘルプ画面の左ボタンを押した時の処理
	public void		function_help_left(){

		nHelpPage--;
		if (nHelpPage <= -1)	nHelpPage = 4;
		nHelpPage %= 5;

		int nLanguageType = gameInfo.getLanguageType ();

		if(nLanguageType == (int)LANGUAGE_TYPE.JAPANESE){
			GameObject.Find ("UI Root/Panel/sgn_help_j").transform.GetComponent<UISprite> ().spriteName = helpPageName[nLanguageType][nHelpPage];
		} else if(nLanguageType == (int)LANGUAGE_TYPE.ENGLISH){
			GameObject.Find ("UI Root/Panel/sgn_help_e").transform.GetComponent<UISprite> ().spriteName = helpPageName[nLanguageType][nHelpPage];
		}
		fInput = false;

	}

	//	ヘルプ画面の右ボタンを押した時の処理
	public void		function_help_right(){

		nHelpPage++;
		nHelpPage %= 5;

		int nLanguageType = gameInfo.getLanguageType ();

		if(nLanguageType == (int)LANGUAGE_TYPE.JAPANESE){
			GameObject.Find ("UI Root/Panel/sgn_help_j").transform.GetComponent<UISprite> ().spriteName = helpPageName[nLanguageType][nHelpPage];
		} else if(nLanguageType == (int)LANGUAGE_TYPE.ENGLISH){
			GameObject.Find ("UI Root/Panel/sgn_help_e").transform.GetComponent<UISprite> ().spriteName = helpPageName[nLanguageType][nHelpPage];
		}

		fInput = false;

	}

	//	タイトル画面に戻るボタンを押した時の処理
	public void		function_help_exit(){

		if(nCount == 0){

			fade	fadeData = GameObject.Find ("Panel").transform.FindChild ("fadeMask").GetComponent<fade>();
			fadeData.setFadeData (1, (int)FADE_TYPE.FADEIN);
			fade.fFade = true;

		}

		nCount++;
		if(nCount >= 60){

			nCount 		= 0;
			nHelpPage 	= 0;
			fInput 		= false;
			Application.LoadLevel ("title");

		}

	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	StageSelect
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	public void function_return(){

		if(nCount == 0){

			//	FadeIn
			GameObject fadeData = GameObject.Find ("Panel").transform.FindChild ("fadeMask").gameObject;
			fadeData.SetActive (true);
			fadeData.GetComponent<fade> ().setFadeData (1, (int)FADE_TYPE.FADEIN);
			fade.fFade = true;

		}

		nCount++;
		if(nCount>=60){
			
			nCount = 0;
			fInput = false;
			Application.LoadLevel ("title");
			
		}

	}

	public void	function_revelAction(){

		if(nCount == 0){

			//	FadeIn
			GameObject fadeData = GameObject.Find ("Panel").transform.FindChild ("fadeMask").gameObject;
			fadeData.SetActive (true);
			fadeData.GetComponent<fade> ().setFadeData (1, (int)FADE_TYPE.FADEIN);
			fade.fFade = true;

		}

		nCount++;
		
		if(nCount>=60){

			//	レベル設定
			string[]	objName = new string[]{
				"stageSelect_lv1", "stageSelect_lv2", "stageSelect_lv3", "stageSelect_lv4", "stageSelect_lv5"
			};
			for(int i = 0 ; i < 5 ; i++){
				if(hitData.collider.transform.parent.name == objName[i]){

					gameInfo.setLevel (i);

				}
			}
			
			nCount = 0;
			fInput = false;
			Application.LoadLevel ("main");
			
		}

	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	Main
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	public void		function_restart(){

		GameObject.Find ("UI Root/Panel/bar_normal").gameObject.SetActive (false);
		GameObject.Find ("Panel").transform.FindChild ("sgn_restart").gameObject.SetActive (true);
		GameObject.Find ("Panel").transform.FindChild ("bar_restart").gameObject.SetActive (true);
		GameObject.Find ("Panel").transform.FindChild ("mask").gameObject.SetActive (true);
		GameObject.Find ("Panel").transform.FindChild ("touchControl").gameObject.SetActive (true);

		fInput = false;

	}

	public void	function_restart_ok(){

		GameObject.Find ("Panel").transform.FindChild ("bar_normal").gameObject.SetActive (true);
		GameObject.Find ("UI Root/Panel/sgn_restart").gameObject.SetActive (false);
		GameObject.Find ("UI Root/Panel/bar_restart").gameObject.SetActive (false);
		GameObject.Find ("UI Root/Panel/mask").gameObject.SetActive (false);
		GameObject.Find ("UI Root/Panel/touchControl").gameObject.SetActive (false);

		int nPlateMax = GameObject.Find ("main").GetComponent<main> ().nPlateMax;
		for(int i = 0 ; i < nPlateMax ; i++){
			
			plate	plateObj = GameObject.Find("UI Root/Panel/plates").transform.GetChild (i).GetComponent<plate>();
			if(plateObj.plateData.nPlateType == (int)PLATE_TYPE.INPUT){
				
				plateObj.plateData.nNumber_Input 	= 0;
				plateObj.plateData.fInput 			= false;
				Transform	plateData = GameObject.Find ("UI Root/Panel/plates").transform.GetChild (i);

				GameObject.Find ("main").GetComponent<main>().plateData[i] = plateObj.plateData;
				plateData.FindChild ("number").GetComponent<UISprite>().spriteName = "info_num_00";
				plateData.FindChild ("number").gameObject.SetActive(false);
				plateData.FindChild ("frame").GetComponent<UISprite>().spriteName = "";
				plateData.FindChild ("frame").GetComponent<frame>().nFrameType = (int)FRAME_TYPE.NONE;
				
			}
			
		}

		fInput = false;

	}

	public void	function_restart_cancel(){

		GameObject.Find ("Panel").transform.FindChild ("bar_normal").gameObject.SetActive (true);
		GameObject.Find ("UI Root/Panel/sgn_restart").gameObject.SetActive (false);
		GameObject.Find ("UI Root/Panel/bar_restart").gameObject.SetActive (false);
		GameObject.Find ("UI Root/Panel/mask").gameObject.SetActive (false);
		GameObject.Find ("UI Root/Panel/touchControl").gameObject.SetActive (false);

		fInput = false;

	}

	public void		function_hint(){

		Transform	panelData = GameObject.Find ("Panel").transform;

		panelData.FindChild ("hintbg").gameObject.SetActive (true);
		int nLanguageType = gameInfo.getLanguageType ();
		if(nLanguageType == (int)LANGUAGE_TYPE.JAPANESE){
			panelData.FindChild ("sgn_hint_j").gameObject.SetActive (true);
		}else if(nLanguageType == (int)LANGUAGE_TYPE.ENGLISH){
			panelData.FindChild ("sgn_hint_e").gameObject.SetActive (true);
		}
		panelData.FindChild ("bar_hint").gameObject.SetActive (true);
		panelData.FindChild ("touchControl").gameObject.SetActive (true);
		panelData.FindChild ("bar_normal").gameObject.SetActive (false);
		helpPageName = new string[][]{

			new	string[]{
				"list1_j", "list2_j", "list3_j", "list4_j"
			},
			new string[]{
				"list1_e", "list2_e", "list3_e", "list4_e"
			}
		};
		nHelpPage = 0;
		fInput = false;

	}

	public void		function_hintLeft(){

		nHelpPage--;
		if (nHelpPage <= -1)	nHelpPage = 3;
		nHelpPage %= 4;

		int nLanguageType = gameInfo.getLanguageType ();
		
		if(nLanguageType == (int)LANGUAGE_TYPE.JAPANESE){
			GameObject.Find ("UI Root/Panel/sgn_hint_j").transform.GetComponent<UISprite> ().spriteName = helpPageName[nLanguageType][nHelpPage];
		} else if(nLanguageType == (int)LANGUAGE_TYPE.ENGLISH){
			GameObject.Find ("UI Root/Panel/sgn_hint_e").transform.GetComponent<UISprite> ().spriteName = helpPageName[nLanguageType][nHelpPage];
		}
		fInput = false;

	}

	public void	function_hintRight(){

		nHelpPage++;
		nHelpPage %= 4;

		int nLanguageType = gameInfo.getLanguageType ();

		if(nLanguageType == (int)LANGUAGE_TYPE.JAPANESE){
			GameObject.Find ("UI Root/Panel/sgn_hint_j").transform.GetComponent<UISprite> ().spriteName = helpPageName[nLanguageType][nHelpPage];
		} else if(nLanguageType == (int)LANGUAGE_TYPE.ENGLISH){
			GameObject.Find ("UI Root/Panel/sgn_hint_e").transform.GetComponent<UISprite> ().spriteName = helpPageName[nLanguageType][nHelpPage];
		}
		fInput = false;

	}

	public void	function_hintExit(){

		GameObject.Find ("Panel").transform.FindChild ("hintbg").gameObject.SetActive (false);
		int nLanguageType = gameInfo.getLanguageType ();
		if(nLanguageType == (int)LANGUAGE_TYPE.JAPANESE){
			GameObject.Find ("UI Root/Panel/sgn_hint_j").gameObject.SetActive (false);
		} else if(nLanguageType == (int)LANGUAGE_TYPE.ENGLISH){
			GameObject.Find ("UI Root/Panel/sgn_hint_e").gameObject.SetActive (false);
		}
		GameObject.Find ("Panel").transform.FindChild ("bar_hint").gameObject.SetActive (false);
		GameObject.Find ("Panel").transform.FindChild ("touchControl").gameObject.SetActive (false);
		GameObject.Find ("Panel").transform.FindChild ("bar_normal").gameObject.SetActive (true);
		fInput = false;

	}

	public void		function_check(){

	/*	if(fStartUp == false){
			fStartUp = true;
			nStartUpCount = PlayerPrefs.GetInt ("StartUpCount_surrender");
			nStartUpCount++;
			PlayerPrefs.SetInt ("StartUpCount_surrender", nStartUpCount);
			Debug.Log ("Count:"+nStartUpCount);				
		}
	*/
		//	lampAnimation
		Transform	lampData = GameObject.Find ("Panel").transform.FindChild ("sgn_lamp");

		//	全て入力されていないなら終了
		if (GameObject.Find ("main").GetComponent<main> ().allInputCheck () == false) {
			fInput = false;
			return;
		}

		if(nAnimationCount == 0){

			//	経過時間を止める
			time.setTimerStartFlag (false);

			//	Debug
	/*		gameObject.AddComponent ("debug");
			if(gameObject.GetComponent<debug>().flag == true){
				gameObject.GetComponent<debug> ().debug_allInput ();
			}
	*/	
			//	複数解答用処理
			int nPlateMax = GameObject.Find ("main").GetComponent<main> ().nPlateMax;
			for(int i = 0 ; i < nPlateMax ; i++){
				
				PLATEDATA	plateData = GameObject.Find ("main").GetComponent<main>().plateData[i];
				
				if(plateData.nPlateType == (int)PLATE_TYPE.INPUT){
					if(plateData.fInput == false){
						
						gameInfo.setClearFlag (false);
						fInput = false;
						
						return;
						
					}
				}
				
			}
			
			if(GameObject.Find ("main").GetComponent<main> ().checkCorrectAnswer () == true){
				
				gameInfo.setClearFlag (true);
				gameInfo.setContinueFlag (false);

				//	不正解
			} else {
				
				gameInfo.setClearFlag (false);
				
			}

			GameObject.Find ("Panel").transform.FindChild ("bar_normal").gameObject.SetActive (false);
			GameObject.Find ("Panel").transform.FindChild ("touchControl").gameObject.SetActive (true);
			lampData.gameObject.SetActive (true);

		}

		nAnimationCount++;

		Transform	decisionData = GameObject.Find ("Panel").transform.FindChild ("sgn_decision");

		if(nAnimationCount >= 0 && nAnimationCount <= 37){

			lampData.GetComponent<UISprite>().width+=4;
			lampData.GetComponent<UISprite>().height+=4;

		//	Rotation
		} else if(nAnimationCount >= 80 && nAnimationCount < 120){
			if(nAnimationCount == 80){

				audioManager	audioData = GameObject.Find ("audioManager").GetComponent<audioManager>();
				audioData.createAudio ((int)AUDIO_TYPE.JGL, (int)AUDIO_JGL.CHECK);

			}
			//	４５度ずつ回転
			if(nAnimationCount % 5 == 0)	lampData.transform.Rotate(0, 0, -45);

		//	spriteAnimation
		} else if(nAnimationCount >= 126 && nAnimationCount < 228){
			if(nAnimationCount % 6 == 0){
				
				nType++;
				nType %= 2;
				lampData.GetComponent<UISprite>().spriteName = "sgn_lamp"+nType.ToString ();
				
			}
			if(nAnimationCount == 228)	lampData.GetComponent<UISprite>().spriteName = "sgn_lamp0";
		} else if(nAnimationCount == 294){
			audioManager	audioData = GameObject.Find ("audioManager").GetComponent<audioManager>();
			if(gameInfo.getClearFlag () == true){

				audioData.createAudio ((int)AUDIO_TYPE.SE, (int)AUDIO_SE.LIGHT_CORRECT);
				lampData.GetComponent<UISprite>().spriteName = "sgn_lamp2a";
				//	クリア回数を１増やす
				gameInfo.addClearCount (gameInfo.getLevel ());

			} else {

				audioData.createAudio ((int)AUDIO_TYPE.SE, (int)AUDIO_SE.LIGHT_INCORRECT);
				lampData.GetComponent<UISprite>().spriteName = "sgn_lamp2b";
				//	不正解カウンタ増加
				gameInfo.addInCorrectCount ();

			}
		} else if(nAnimationCount == 354){

			lampData.gameObject.SetActive (false);
			lampData.GetComponent<UISprite>().width 		= 2;
			lampData.GetComponent<UISprite>().height 		= 2;
			lampData.GetComponent<UISprite>().spriteName 	= "sgn_lamp0";

		} else if(nAnimationCount >= 364 && nAnimationCount < 412){
			if(nAnimationCount == 364){

				audioManager	audioData = GameObject.Find ("audioManager").GetComponent<audioManager>();

				decisionData.gameObject.SetActive (true);

				if(gameInfo.getClearFlag () == true){
					if(gameInfo.getInCorrectCount () == 0){
				
						audioData.createAudio ((int)AUDIO_TYPE.JGL, (int)AUDIO_JGL.CORRECT);
						decisionData.GetComponent<UISprite>().spriteName = "sgn_maru1";
			
					} else {

						audioData.createAudio ((int)AUDIO_TYPE.JGL, (int)AUDIO_JGL.CORRECT);
						decisionData.GetComponent<UISprite>().spriteName = "sgn_maru2";

					}
				} else {
					audioData.createAudio ((int)AUDIO_TYPE.JGL, (int)AUDIO_JGL.INCORRECT);
					decisionData.GetComponent<UISprite>().spriteName = "sgn_batu";
				}
			}

			decisionData.GetComponent<UISprite>().width+=10;
			decisionData.GetComponent<UISprite>().height+=10;

		} else if(nAnimationCount == 592){

			decisionData.GetComponent<UISprite>().width 	= 2;
			decisionData.GetComponent<UISprite>().height 	= 2;
			decisionData.gameObject.SetActive (false);
			lampData.GetComponent<UISprite>().width 	= 2;
			lampData.GetComponent<UISprite>().height 	= 2;
			lampData.gameObject.SetActive (false);

			if(gameInfo.getClearFlag ()){
			/*	if(gameInfo.getShowInterstitialFlag() == false){
					gameInfo.setShowInterstitialFlag (true);
				}
			*/
				GameObject.Find ("Panel").transform.FindChild ("bar_next").gameObject.SetActive (true);
				GameObject.Find ("Panel").transform.FindChild("mask").gameObject.SetActive (true);
				fInput = false;
			} else {

				time.setTimerStartFlag (true);
				GameObject.Find ("UI Root/Panel/touchControl").gameObject.SetActive (false);
				GameObject.Find ("Panel").transform.FindChild ("bar_normal").gameObject.SetActive (true);
				nAnimationCount = 0;
				fInput = false;

			}
		} else if(nAnimationCount >= 592){
	/*		if(GoogleMobileAdsDemoScript.getEndFlag() == true){
				nAnimationCount = 0;
				gameInfo.setShowInterstitialFlag(false);
				GoogleMobileAdsDemoScript.setEndFlag(false);
				gameInfo.setContinueFlag (false);
				gameInfo.savePref_continueFlag (false);
				gameInfo.setInCorrectCount(0);
				gameInfo.savePref_inCorrectCount ();
				gameInfo.setClearFlag (false);
				gameInfo.setGameStartFlag (false);
				Application.LoadLevel ("stageSelect");
				fInput = false;
			}
	*/	}

	}

	public void		function_next(){

		if(gameInfo.getShowInterstitialFlag() == false){
			gameInfo.setShowInterstitialFlag (true);
		}
		if(GoogleMobileAdsDemoScript.getEndFlag() == true){
			nAnimationCount = 0;
			gameInfo.setShowInterstitialFlag(false);
			GoogleMobileAdsDemoScript.setEndFlag(false);
			gameInfo.setContinueFlag (false);
			gameInfo.savePref_continueFlag (false);
			gameInfo.setInCorrectCount(0);
			gameInfo.savePref_inCorrectCount ();
			gameInfo.setClearFlag (false);
			gameInfo.setGameStartFlag (false);
			Application.LoadLevel ("stageSelect");
			fInput = false;
		}
		
	}
	
	public void		function_bgm(){

		audioManager	audioData 	= GameObject.Find ("audioManager").GetComponent<audioManager> ();
		string[] 		audioName 	= new string[]{
			"btn_bgmoff", "btn_bgmon1", "btn_bgmon2", "btn_bgmon3"
		};
		int[] 			nBGMIndex 	= new int[]{
			-1, 0, 1, 2
		};

		if(nBGMIndex[nBgmType] != -1){
			audioData.audioDestroy (audioData.audioName[(int)AUDIO_TYPE.BGM][nBGMIndex[nBgmType]]);
		}

		nBgmType++;
		nBgmType %= audioName.Length;

		if(nBGMIndex[nBgmType] != -1){
			audioData.createAudio ((int)AUDIO_TYPE.BGM, nBGMIndex[nBgmType]);
		}

		GameObject.Find ("UI Root/Panel/btn_bgm/Background").GetComponent<UISprite>().spriteName = audioName[nBgmType];
		fInput = false;

	}

	public void		function_mainSurrender(){

		Transform panelData = GameObject.Find ("Panel").transform;

		panelData.FindChild ("bar_normal").gameObject.SetActive (false);
		panelData.FindChild ("bar_number").gameObject.SetActive (false);
		panelData.FindChild ("bar_surrender").gameObject.SetActive (true);
		panelData.FindChild ("mask").gameObject.SetActive (true);
		panelData.FindChild ("touchControl").gameObject.SetActive (true);

		fInput = false;
		
	}

	public void		function_surrender_ok(){

		nCount++;
		if(fStartUp == false){

			fStartUp = true;
			nStartUpCount = PlayerPrefs.GetInt ("StartUpCount_surrender");
			nStartUpCount++;
			PlayerPrefs.SetInt ("StartUpCount_surrender", nStartUpCount);

		}
		if(nStartUpCount == nShowCount){

			GameObject.Find ("main").GetComponent<main>().plate_Init ();

			gameInfo.setContinueFlag(false);
			gameInfo.savePref_continueFlag (false);

			PlayerPrefs.SetInt ("StartUpCount_surrender", 0);
			if(gameInfo.getShowInterstitialFlag() == false){
				gameInfo.setShowInterstitialFlag (true);
			}
			if(GoogleMobileAdsDemoScript.getEndFlag() == true){

				gameInfo.setShowInterstitialFlag(false);
				GoogleMobileAdsDemoScript.setEndFlag(false);
				nCount = 0;
				fInput = false;
				time.setTimerStartFlag (false);
				time.setTime (0.0f);
				gameInfo.setGameStartFlag(false);
				gameInfo.setInCorrectCount (0);
				gameInfo.savePref_inCorrectCount ();
				Application.LoadLevel ("title");
				
			}
		} else {

			gameInfo.setContinueFlag(false);
			gameInfo.savePref_continueFlag (false);

			GameObject.Find ("main").GetComponent<main>().plate_Init ();

			nCount++;
			if(nCount >= 60){

				nCount = 0;
				time.setTimerStartFlag (false);
				time.setTime (0.0f);
				gameInfo.setGameStartFlag(false);
				gameInfo.setInCorrectCount (0);
				gameInfo.savePref_inCorrectCount ();
				fInput = false;
				Application.LoadLevel ("title");

			}
		}

	}
	
	public void		function_surrender_cancel(){

		fInput = false;
		GameObject.Find ("UI Root/Panel/bar_surrender").gameObject.SetActive (false);
		GameObject.Find ("UI Root/Panel/mask").gameObject.SetActive(false);
		GameObject.Find ("UI Root/Panel/touchControl").gameObject.SetActive (false);
		GameObject.Find ("Panel").transform.FindChild ("bar_normal").gameObject.SetActive (true);

	}

	public void		function_delete(){
	
		plateObj_op.plateData.fInput 		= false;
		plateObj_op.plateData.nNumber_Input = 0;
		plateObj_op.transform.FindChild ("number").GetComponent<UISprite> ().spriteName = "info_num_01";
		plateObj_op.transform.FindChild ("number").gameObject.SetActive (false);
		plateObj_op.transform.FindChild ("frame").GetComponent<frame> ().nFrameType = (int)FRAME_TYPE.NONE;
		plateObj_op.transform.FindChild ("frame").GetComponent<UISprite> ().spriteName = "";
		plateObj_op.transform.FindChild ("btn_delete").gameObject.SetActive (false);

		GameObject.Find ("Panel").transform.FindChild ("bar_number").gameObject.SetActive (false);
		GameObject.Find ("Panel").transform.FindChild ("bar_normal").gameObject.SetActive (true);

		int nIndex = plateObj_op.plateData.nPos;
		GameObject.Find ("main").GetComponent<main> ().plateData [nIndex].fInput 		= false;
		GameObject.Find ("main").GetComponent<main> ().plateData [nIndex].nNumber_Input = 0;

		fInput = false;

	}

}
