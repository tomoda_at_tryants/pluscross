﻿using UnityEngine;
using System.Collections;

public class help : MonoBehaviour {

	// Use this for initialization
	void Start () {

		gameInfo.setSceneNum ((int)SCENE.HELP);
		GameObject.Find ("UI Root/touchEvent").GetComponent<touchEvent> ().nHelpPage = 0;

		int nLanguageType = gameInfo.getLanguageType ();

		if(nLanguageType == (int)LANGUAGE_TYPE.JAPANESE){
			GameObject.Find ("Panel").transform.FindChild ("sgn_help_j").gameObject.SetActive (true);
			GameObject.Find ("Panel").transform.FindChild ("sgn_help_e").gameObject.SetActive (false);
		} else if(nLanguageType == (int)LANGUAGE_TYPE.ENGLISH){
			GameObject.Find ("Panel").transform.FindChild ("sgn_help_e").gameObject.SetActive (true);
			GameObject.Find ("Panel").transform.FindChild ("sgn_help_j").gameObject.SetActive (false);
		}

		//	bar
		float fManualHeight = 0.0f;
		float 	ap 			= Screen.height * 1.0f / Screen.width * 1.0f;
		fManualHeight 		= (int)( 640.0f * ap );
		float	fSize = fManualHeight -960.0f;
		int nMax = GameObject.Find ("Panel").transform.childCount;
		for(int i = 0 ; i < nMax ; i++){
			
			Transform	childData = GameObject.Find ("Panel").transform.GetChild (i);
			
			if(childData.tag == "bar"){
				childData.localPosition = new Vector3(childData.localPosition.x, childData.localPosition.y-fSize, childData.localPosition.z);
			}
		}


		//	FadeOut
		GameObject fadeData = GameObject.Find ("Panel").transform.FindChild ("fadeMask").gameObject;
		fadeData.SetActive (true);
		fadeData.GetComponent<fade> ().setFadeData (1, (int)FADE_TYPE.FADEOUT);
		fade.fFade = true;
	
	}

	// Update is called once per frame
	void Update () {
	
	}
}
