﻿using UnityEngine;
using System.Collections;
using LitJson;
using System;

public enum PLATE_TYPE{
	INPUT, BLACK, HINT
}

public	enum AREA{
	ROW, COL
}

//	マスデータ
public class PLATEDATA{

	public int		nPos;
	public int		nRowPos;
	public int		nColPos;
	public int		nPlateType;
	public int		nNumber_Answer;
	public int 		nNumber_Input;
	public int		nRowNumber;
	public int		nColNumber;
	public int		nRowCount;
	public int 		nColCount;
	public int		nUseNumberFlag;
	public int 		nUseNumberFlag_op;
	public bool		fTouch;
	public bool		fInput;
	public bool		fSetFlag;
	public bool		fDefineNumber;

	public PLATEDATA( int nPos, int nRowPos, int nColPos, int nPlateType, int nNumber_Answer, int nNumber_Input, int nRowNumber, int nColNumber, int nRowCount, int nColCount, int nUseNumberFlag){

		this.nPos 				= nPos;
		this.nRowPos 			= nRowPos;
		this.nColPos 			= nColPos;
		this.nPlateType 		= nPlateType;
		this.nNumber_Answer 	= nNumber_Answer;
		this.nNumber_Input		= nNumber_Input;
		this.nRowNumber 		= nRowNumber;
		this.nColNumber 		= nColNumber;
		this.nRowCount 			= nRowCount;
		this.nColCount 			= nColCount;
		this.nUseNumberFlag 	= nUseNumberFlag;
		this.nUseNumberFlag_op 	= nUseNumberFlag;

	}
	
}

public class main : MonoBehaviour {

	public 	int[]			nBoard;				//	盤データ
	public 	int[]			nStartPos;			//	最初の入力マスの位置
	public	int				nPlateMax;			//	マス数
	public	int				nRowMax;			//	横のマス数
	public	int				nColMax;			//	縦のマス数
	public  bool			fCheckComplate;		//	全データ管理フラグ
	public 	PLATEDATA[]		plateData;			//	作業用マスデータ
	public static 	int[]	nStageTypeCount;	

	//	プレハブ
	public	GameObject	platePrefab;
	public	GameObject	numberPrefab; 

	//	JSONデータ
	public LitJson.JsonData	json_useNumber;
	public LitJson.JsonData json_plateData;

	public int	nCount;
	
	// Use this for initialization
	void Start () {
	
		//	シーン値を「main」に設定
		gameInfo.setSceneNum ((int)SCENE.MAIN);

		gameInfo.objName_init ();

		//	マスの各最大行列値と最大マス数設定
		nRowMax 	= 11;	
		nColMax 	= 11;
		nPlateMax 	= nRowMax*nColMax;
		//	各ステージタイプ値設定
		nStageTypeCount = new int[]{
			10, 10, 10, 10, 10
		};

		int nLevel = 0, nType = 0;
		//	//	コンティニューを行うならデータのロード
		if(PlayerPrefs.GetInt ("fContinue") == 1){

			nLevel 	= gameInfo.loadPref_level ();
			nType 	= gameInfo.loadPref_stageType ();

		//	コンティニューしないなら各データ設定
		} else {

			nLevel 			= gameInfo.getLevel ();
			bool	flag 	= false;

			//前回行った問題のタイプと同じにならないようにする
			do{
				nType = UnityEngine.Random.Range (0, nStageTypeCount[nLevel]);
				if(nType != gameInfo.getFormerStageType (nLevel)){
					flag = true;
				}
			}while(flag == false);
			gameInfo.setFormerStageType (nLevel, nType);

		}
		gameInfo. setLevel (nLevel);
		gameInfo.setStageType (nType);

		//	json_useNumber
		string 		readData 	= "";
		TextAsset 	txt 		= Resources.Load ("Text/useNumber") as TextAsset;
		readData 				= txt.text;
		json_useNumber 			= LitJson.JsonMapper.ToObject (readData);
		//	json_plateData
		txt 					= Resources.Load ("Text/plateData") as TextAsset;
		readData 				= txt.text;
		json_plateData 			= LitJson.JsonMapper.ToObject (readData);

		//	パネルの生成
		createPanel ();

		//	設定している言語によってスプライトを変更する
		int 		nLanguageType 		= PlayerPrefs.GetInt ("languageType");
		string[]	spriteName_check 	= new string[]{
			"sgn_btn_answer_j", "sgn_btn_answer_e"
		};
		GameObject.Find ("UI Root/Panel/bar_normal/btn_check/sgn_answer").GetComponent<UISprite>().spriteName = spriteName_check[nLanguageType];

		string[]	spriteName_surrender = new string[]{
			"sgn_surrender_j", "sgn_surrender_e"
		};
		GameObject.Find ("Panel").transform.FindChild ("bar_surrender").FindChild ("sgn_surrender").GetComponent<UISprite>().spriteName = spriteName_surrender[nLanguageType];

		string[]	spriteName_restart = new string[]{
			"wnd_restart_j", "wnd_restart_e"
		};
		GameObject.Find ("Panel").transform.FindChild ("sgn_restart").GetComponent<UISprite>().spriteName = spriteName_restart[nLanguageType];

		string[]	spriteName_create = new string[]{
			"sgn_create_j", "sgn_create_e"
		};
		GameObject.Find ("Panel").transform.FindChild ("sgn_create").GetComponent<UISprite>().spriteName = spriteName_create[nLanguageType];
		GameObject.Find ("Panel").transform.FindChild ("sgn_create").gameObject.SetActive (true);

		string 	setSpriteName 	= "sgn_maze" + (nLevel + 1).ToString ();
		GameObject.Find ("UI Root/Panel/sgn_level").GetComponent<UISprite> ().spriteName = setSpriteName;

		//	checkSprite
		if(allInputCheck () == true){
			GameObject.Find ("UI Root/Panel/bar_normal/btn_check/Background").GetComponent<UISprite>().spriteName = "btn_check";
		} else {
			GameObject.Find ("UI Root/Panel/bar_normal/btn_check/Background").GetComponent<UISprite>().spriteName = "btn_check_off";
		}

		//	bar
		float fManualHeight = 0.0f;
		float 	ap 			= Screen.height * 1.0f / Screen.width * 1.0f;
		fManualHeight 		= (int)( 640.0f * ap );
		float		fSize 	= (fManualHeight -960.0f)/2;
		int nMax = GameObject.Find ("Panel").transform.childCount;
		for(int i = 0 ; i < nMax ; i++){

			Transform	childData = GameObject.Find ("Panel").transform.GetChild (i);

			if(childData.tag == "bar"){
				if(childData.name != "bar_surrender"){
					childData.localPosition = new Vector3(childData.localPosition.x, childData.localPosition.y-fSize, childData.localPosition.z);
				}
			}
		}

		time.setTime (0.0f);

		//	FadeOut
/*		GameObject fadeData = GameObject.Find ("Panel").transform.FindChild ("fadeMask").gameObject;
		fadeData.SetActive (true);
		fadeData.GetComponent<fade> ().setFadeData (1, (int)FADE_TYPE.FADEOUT);
		fade.fFade = true;
*/	
		//	mask
		GameObject.Find ("Panel").transform.FindChild ("mask").gameObject.SetActive (true);

		fCheckComplate = false;
		if(gameInfo.getContinueFlag () == false){
			GameObject.Find ("Panel").transform.FindChild ("touchControl").gameObject.SetActive (true);
			gameInfo.setLoadingFlag (true);
		} else {
			GameObject.Find ("Panel").transform.FindChild ("touchControl").gameObject.SetActive (false);
			gameInfo.setLoadingFlag (false);
		}
	
	}

	public void	plate_Init(){

		fCheckComplate = false;
		for(int i = 0 ; i < nPlateMax ; i++){
			GameObject.Find ("UI Root/Panel/plates").transform.GetChild (i).GetComponent<plate>().Init ();
		}

	}

	/// <summary>
	/// 問題の生成を行う
	/// </summary>
/*	void	createKakkuro(){
		
		for(int i = 0 ; i < nPlateMax ; i++){
			if(plateData[i].nPlateType == (int)PLATE_TYPE.HINT){
				//	横のヒントマス設定
				if(plateData[i].nRowCount>0){

			//		int		nNumber = setHintNumber (i, (int)AREA.ROW);
			//		plateData[i].nRowNumber = nNumber;

					//	入力可能な数字のフラグを入力マスに設定
					for(int j = 1 ; j <= plateData[i].nRowCount ; j++){

						setUseNumberFlag (i+j, getRowFlag (i));

					}
					
				}
				//	縦のヒントマス設定
				if(plateData[i].nColCount>0){
			
					//	ヒント値の生成・設定
			//		int		nNumber = setHintNumber (i, (int)AREA.COL);
			//		plateData[i].nColNumber = nNumber;
				
					//	入力可能な数字のフラグを入力マスに設定
					for(int j = 1 ; j <= plateData[i].nColCount ; j++){

						setUseNumberFlag (i+(nRowMax*j), getColFlag (i));
						
					}
					
				}
				//	確定している値があるかチェック
				checkDefineNumber ();
			}
		}
		
	}

	/// <summary>
	///	入力可能な数字が一つしか無い入力マスを調べ、確定させる
	/// </summary>
	void	checkDefineNumber(){

		bool	flag 		= false;
		do{

			int 	nSetCount 	= 0;

			for(int i = 0 ; i < nPlateMax ; i++){
				if(plateData[i].nPlateType == (int)PLATE_TYPE.INPUT){
					if(plateData[i].fDefineNumber == false){

						int	nNumber = 0;
						int	nCount 	= 0;
						int	nBuf 	= 0x000000001;
						
						for(int j = 0 ; j < 9 ; j++){
							if(((plateData[i].nUseNumberFlag_op>>j) & nBuf) == 1){
								
								nCount++;
								nNumber = j+1;
								if(nCount > 1)	break;

							}
						}
						//	入力可能数字確定
						if(nCount == 1){

			//				print ("kakutei:"+i);
			//				print ("Flag:"+Convert.ToString (plateData[i].nUseNumberFlag_op, 2).PadLeft (9, '0'));
							nSetCount++;
							plateData[i].fDefineNumber 	= true;
							plateData[i].nNumber_Answer = nNumber;
							setUseNumberFlagOff(i, nNumber);
							
						} else if(nCount == 0){
		
							return;

						}

					}
				}
				if(i == nPlateMax-1){
					if(nSetCount == 0){

						flag = true;

					}
				}
			}
		}while(flag == false);

	}

	void	setUseNumberFlagOff(int nPos, int nNumber){

		int nIndex 	= 0;
		int nFlag	= 0x000000000;
		int nBuf 	= 0x000000001;

		for(int i = 0 ; i < nRowMax ; i++){
			int	nHintPos = i+(plateData[nPos].nColPos*nRowMax);
			if(checkArea (nHintPos, nPos, (int)AREA.ROW) == true){
				for(int j = plateData[nHintPos].nRowPos+1 ; j < plateData[nHintPos].nRowCount ; j++){
				
					nIndex 	= j+(plateData[nHintPos].nColPos*nRowMax);
					nFlag 	= plateData[nIndex].nUseNumberFlag_op;
					nBuf 	= 0x000000001<<(nNumber-1);

					nFlag &= ~nBuf;
					plateData[nIndex].nUseNumberFlag_op = nFlag;

				}
			}
		}

		for(int i = 0 ; i < nColMax ; i++){
			int	nHintPos = plateData[nPos].nRowPos+(i*nRowMax);
			if(checkArea (nHintPos, nPos, (int)AREA.COL) == true){
				for(int j = plateData[nHintPos].nColPos+1 ; j < plateData[nHintPos].nColCount ; j++){

					nIndex 	= plateData[nHintPos].nRowPos+(j*nRowMax);
					nFlag 	= plateData[nIndex].nUseNumberFlag_op;
					nBuf 	= 0x000000001<<(nNumber-1);

					nFlag &= ~nBuf;
					plateData[nIndex].nUseNumberFlag_op = nFlag;
					
				}
			}
		}

	}

	/// <summary>
	///	入力可能な数字の数を返す
	/// </summary>
	int	getDefineNumberCount(int nFlag){

		int nCount = 0;
		int nBuf = 0x000000001;

		for(int j = 0 ; j < 9 ; j++){
			if(((nFlag>>j)&nBuf) == 1){

				nCount++;

			}
		}
	
		return	nCount;

	}

	/// <summary>
	///	設定したいヒント数字が関連するヒントマスに使用されていないか調べる	
	/// </summary>
	bool	checkUseHintNumber(int nPos, int nHintNumber, int nDirection){

		if (nHintNumber <= 0)										return	false;
		if (nPos < 0 || nPos > nPlateMax - 1)						return	false;
		if (plateData [nPos].nPlateType != (int)PLATE_TYPE.HINT)	return	false;

		if (nDirection == (int)AREA.ROW) {
			//	調べたいヒントマスの下にあるマスを調べる
			for(int i = 1 ; i < nColMax-plateData[nPos].nColPos ; i++){

				int nIndex = nPos+(i*nRowMax);
				if(plateData[nIndex].nPlateType == (int)PLATE_TYPE.HINT){
					if(plateData[nPos].nRowCount == plateData[nIndex].nRowCount){
						if(nHintNumber == plateData[nIndex].nRowNumber){

							return	false;

						}
					}
				} else {

					break;

				}
			}
			//	調べたいヒントマスの上にあるマスを調べる
			for(int i = 1 ; i <= nColMax-(nColMax-plateData[nPos].nColPos) ; i++){

				int	nIndex = nPos-(i*nRowMax);
				if(plateData[nIndex].nPlateType == (int)PLATE_TYPE.HINT){
					if(plateData[nPos].nRowCount == plateData[nIndex].nRowCount){
						if(nHintNumber == plateData[nIndex].nRowNumber){

							return	false;
							
						}
					}
				} else {
					
					break;
					
				}
			}
		} else {
			//	調べたいヒントマスの右にあるマスを調べる
			for(int i = 1 ; i < nRowMax-plateData[nPos].nRowPos ; i++){
				
				int nIndex = nPos+i;
				if(plateData[nIndex].nPlateType == (int)PLATE_TYPE.HINT){
					if(plateData[nPos].nColCount == plateData[nIndex].nColCount){
						if(nHintNumber == plateData[nIndex].nColNumber){
							
							return	false;
							
						}
					}
				} else {
					
					break;
					
				}
			}
			//	調べたいヒントマスの左にあるマスを調べる
			for(int i = 1 ; i <= nRowMax-(nRowMax-plateData[nPos].nRowPos) ; i++){
				
				int	nIndex = nPos-i;
				if(plateData[nIndex].nPlateType == (int)PLATE_TYPE.HINT){
					if(plateData[nPos].nColCount == plateData[nIndex].nColCount){
						if(nHintNumber == plateData[nIndex].nColNumber){
							
							return	false;
							
						}
					}
				} else {
					
					break;
					
				}
			}
		}

		return	true;

	}

	void	setUseNumberFlag(int nPos, int nFlag){

		if(plateData[nPos].fSetFlag == false){

			plateData[nPos].fSetFlag 			= true;
			plateData[nPos].nUseNumberFlag_op 	= nFlag;
//			Debug.Log ("Pos:"+nPos + "     Flag:"+Convert.ToString (nFlag, 2).PadLeft (9, '0'));

		} else {

			plateData[nPos].nUseNumberFlag_op &= nFlag;
			if(plateData[nPos].nUseNumberFlag_op == 0x000000000){

//				Debug.Log ("Error" + "    Pos:"+nPos);

			}

		}

	}
*/	

	void	createStageData_continue(){

		//	非表示
		GameObject.Find ("Panel").transform.FindChild ("sgn_create").gameObject.SetActive (false);
		GameObject.Find ("Panel").transform.FindChild ("mask").gameObject.SetActive (false);
		
		//	セーブしたデータのロードを行う
		gameObject.AddComponent("saveLoad").GetComponent<saveLoad>().loadStageData ();
		
		//	保存した入力マスのデータを設定する
		for(int i = 0 ; i < nPlateMax ; i++){
			if(plateData[i].nPlateType == (int)PLATE_TYPE.INPUT){
				if(plateData[i].nNumber_Input > 0){
					
					plate	plateObj 			= GameObject.Find ("UI Root/Panel/plates").transform.GetChild (i).GetComponent<plate>();
					plateObj.plateData 			= plateData[i];
					plateObj.plateData.fInput 	= true;
					plateObj.transform.FindChild ("number").GetComponent<UISprite>().spriteName = "info_num_0"+plateObj.plateData.nNumber_Answer.ToString ()+"w";
					//	フレームデータの設定
					plateObj.transform.FindChild ("frame").gameObject.SetActive (true);
					string[]	frameName = new string[]{
						"", "sgn_plate_clear", "sgn_plate_unclear"
					};
					int	nFrameType = plateObj.transform.FindChild ("frame").GetComponent<frame>().nFrameType;
					plateObj.transform.FindChild ("frame").GetComponent<UISprite>().spriteName = frameName[nFrameType];
					
				}
			}
		}
		fCheckComplate = true;
		
		//	マスデータの初期化・設定を行う
	//	setPanelData ();
		
	}

	void	createStageData_normal(){

		while(true){
			
			//	ヒントデータの設定
			setHintData ();
			//	ヒントデータが正しく設定されていないならもう一度
			if(checkHint() == false)	continue;
			
			e (0);
			break;
			
		}
		
		//	createColNumber
		for(int i = 0 ; i < nPlateMax ; i++){
			if(plateData[i].nColCount > 0){
				
				int		nNum 	= 0;
				
				for(int j = plateData[i].nColPos+1 ; j <= plateData[i].nColPos+plateData[i].nColCount ; j++){
					
					int	nElement = (j*nRowMax)+plateData[i].nRowPos;
					nNum += plateData[nElement].nNumber_Answer;
					if(j == plateData[i].nColPos+plateData[i].nColCount){
						
						plateData[i].nColNumber = nNum;
						
					}
				}
			}
		}
		
		for(int i = 0 ; i < nPlateMax ; i++){
			//	checkAnswer
			if(plateData[i].nPlateType == (int)PLATE_TYPE.INPUT){
				for(int j = 0 ; j < nColMax ; j++){
					if(checkArea(((j*nColMax)+plateData[i].nRowPos), i, (int)AREA.COL)){
						for(int k = plateData[i].nColPos+1 ; k <= j+plateData[(j*nColMax)+plateData[i].nRowPos].nColCount ; k++){
							if(plateData[i].nNumber_Answer == plateData[(k*nColMax)+plateData[i].nRowPos].nNumber_Answer){
								
								fCheckComplate = false;
								return;
								
							}
						}
					}
				}
			}
			//	checkColNumber
			if(plateData[i].nColCount > 0){
				for(int j = plateData[i].nRowPos+1 ; j < nRowMax ; j++){
					if(plateData[i].nColCount == plateData[j+(nRowMax*plateData[i].nColPos)].nColCount){
						if(plateData[i].nColNumber == plateData[j+(nRowMax*plateData[i].nColPos)].nColNumber){
							
							fCheckComplate = false;
							return;
							
						}
					}
					if(plateData[j+(nRowMax*plateData[i].nColPos)].nColCount == 0){
						break;
					}
				}
				
			}
			if(i == nPlateMax-1)	fCheckComplate = true;
			
		}
		
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void Update () {
	
		if(gameInfo.getGameStartFlag () == false){
			//	ローディングを行うならローディングカウンタ増加
			if(gameInfo.getLoadingFlag () == true){
				gameInfo.setLoadingCount (gameInfo.getLoadingCount ()+1);
			}

			//	ステージに必要なデータが準備できていないなら
			if(fCheckComplate == false){
				//	コンティニューデータがある場合
				if(PlayerPrefs.GetInt ("fContinue") == 1){
					createStageData_continue ();
					gameInfo.setLoadingCount(180);
				} else {
					createStageData_normal ();
				}
			}
		
			//	ステージデータの準備ができたら各データ設定
			if(fCheckComplate == true){
				if(gameInfo.getLoadingFlag () == true){
					if(gameInfo.getLoadingCount() >= 180){

						GameObject.Find ("Panel").transform.FindChild ("touchControl").gameObject.SetActive (false);
						GameObject.Find ("Panel").transform.FindChild ("sgn_create").gameObject.SetActive (false);
						GameObject.Find ("Panel").transform.FindChild ("mask").gameObject.SetActive (false);
						time.setTimerStartFlag (true);
						gameInfo.setLoadingFlag (false);
						gameInfo.setLoadingCount(0);
						gameInfo.setGameStartFlag (true);
						setPanelData ();
						
					}
				} else {

					//	ゲーム開始フラグを立てる
					gameInfo.setGameStartFlag (true);
					time.setTimerStartFlag (true);
					//	コンティニューフラグを下げる
					gameInfo.setContinueFlag (false);
					PlayerPrefs.SetInt ("fContinue", 0);
					
				}
				fCheckComplate = false;
			}
		}

		//	checkSprite
		if(allInputCheck () == true){
			GameObject.Find ("Panel").transform.FindChild ("bar_normal").FindChild ("btn_check").FindChild ("Background").GetComponent<UISprite>().spriteName = "btn_check";
		} else {
			GameObject.Find ("Panel").transform.FindChild ("bar_normal").FindChild ("btn_check").FindChild ("Background").GetComponent<UISprite>().spriteName = "btn_check_off";
		}

	}

	void	setPanelData(){

		for(int i = 0 ; i < nPlateMax ; i++){
			
			plate	plateObj 	= GameObject.Find ("Panel").transform.FindChild ("plates").GetChild (i).GetComponent<plate>();
			plateObj.plateData 	= plateData[i];
			plateObj.tag 		= "plate";
			
			if(plateObj.plateData.nPlateType == (int)PLATE_TYPE.INPUT){
				
				//		plateObj.transform.FindChild ("number").gameObject.SetActive (true);
				//		Debug.Log ("Pos:"+i + "    Answer:"+plateObj.plateData.nNumber_Answer.ToString ());
				plateObj.transform.FindChild ("number").gameObject.SetActive (false);
				plateObj.transform.FindChild ("number").GetComponent<UISprite>().spriteName = "info_num_0"+plateObj.plateData.nNumber_Answer.ToString ();

			}
			
			if(plateObj.plateData.nRowCount > 0){
				if(plateObj.plateData.nRowNumber <= 9){
					
					plateObj.transform.FindChild ("hintNumber_row_00").gameObject.SetActive(true);
					plateObj.transform.FindChild ("hintNumber_row_00").GetComponent<UISprite>().spriteName = "info_num_0"+plateObj.plateData.nRowNumber.ToString ()+"w";
					plateObj.transform.FindChild ("hintNumber_row_01").gameObject.SetActive(false);
					
				} else {
					int	nNum = 0;
					//	１の位を求める
					plateObj.transform.FindChild ("hintNumber_row_00").gameObject.SetActive(true);
					nNum = plateObj.plateData.nRowNumber % 10;
					plateObj.transform.FindChild ("hintNumber_row_00").GetComponent<UISprite>().spriteName = "info_num_0"+nNum.ToString ()+"w";
					//	１０の位を求める
					plateObj.transform.FindChild ("hintNumber_row_01").gameObject.SetActive(true);
					nNum = plateObj.plateData.nRowNumber / 10;
					plateObj.transform.FindChild ("hintNumber_row_01").GetComponent<UISprite>().spriteName = "info_num_0"+nNum.ToString ()+"w";
					
				}
			}
			
			if(plateObj.plateData.nColCount > 0){
				if(plateObj.plateData.nColNumber <= 9){
					
					plateObj.transform.FindChild ("hintNumber_col_00").gameObject.SetActive (true);						
					plateObj.transform.FindChild ("hintNumber_col_00").GetComponent<UISprite>().spriteName = "info_num_0"+plateObj.plateData.nColNumber.ToString ()+"w";
					plateObj.transform.FindChild ("hintNumber_col_01").gameObject.SetActive(false);
					
				} else {
					
					int	nNum = 0;
					//	１の位を求める
					plateObj.transform.FindChild ("hintNumber_col_00").gameObject.SetActive(true);
					nNum = plateObj.plateData.nColNumber % 10;
					plateObj.transform.FindChild ("hintNumber_col_00").GetComponent<UISprite>().spriteName = "info_num_0"+nNum.ToString ()+"w";
					//	１０の位を求める
					plateObj.transform.FindChild ("hintNumber_col_01").gameObject.SetActive(true);
					nNum = plateObj.plateData.nColNumber / 10;
					plateObj.transform.FindChild ("hintNumber_col_01").GetComponent<UISprite>().spriteName = "info_num_0"+nNum.ToString ()+"w";
					
				}
			}
			
		}

	}

	public	bool	allInputCheck(){

		for(int i = 0 ; i < nPlateMax ; i++){
			if(plateData[i].nPlateType == (int)PLATE_TYPE.INPUT){
				if(plateData[i].fInput == false){
					if(plateData[i].nNumber_Input == 0){
						return	false;
					}
				}
			}
		}
		return	true;

	}
	
	void	setHintData(){

		for(int i = 0 ; i < nPlateMax ; i++){
			if(plateData[i].nPlateType == (int)PLATE_TYPE.HINT){
				//	横のヒントマス設定
				if(plateData[i].nRowCount>0){
					int		nNumber = setHintNumber (plateData[i].nRowCount);
					plateData[i].nRowNumber = nNumber;
				}
				//	縦のヒントマス設定
				if(plateData[i].nColCount>0){
					int		nNumber = setHintNumber (plateData[i].nColCount);
					plateData[i].nColNumber = nNumber;
				}
				
				int nFlag = 0x000000000;
				//	入力マスに入力可能な値をフラグで管理
				if(plateData[i].nRowCount > 0 && plateData[i].nRowNumber > 0){
					
					nFlag = getRowFlag (i);
					plateData[i].nUseNumberFlag_op = nFlag;
					
				}
				
			}
		}
		
	}
	
	bool	checkHint(){
		
		for(int i = 0 ; i < nPlateMax ; i++){
			if(plateData[i].nPlateType == (int)PLATE_TYPE.HINT){
				if(hintCheck(i) == false)	return	false;
			}
		}
		
		return	true;
		
	}
	
	void	e(int nPos){
		
		if (nPos >= nPlateMax)	return;
		
		int 		nRow 		= nPos % nRowMax;	
		int 		nCol 		= nPos / nColMax;
		int 		nSum 		= 0;
		ArrayList 	NumberList 	= new ArrayList ();
		NumberList 				= createNumberList (nPos);
		
		if(plateData[nPos].nPlateType != (int)PLATE_TYPE.HINT ||
		   plateData[nPos].nRowCount == 0){
			
			e (nPos+1);
			
		} else {
			
			bool	flag = false;
			do{
				for(int i = nRow+1 ; i <= nRow+plateData[nPos].nRowCount ; i++){
					
					int		nElement = UnityEngine.Random.Range (0, NumberList.Count);
					//					Debug.Log("Pos:"+(i+(nCol*nRowMax)) + "    Count:"+NumberList.Count + "    Number:"+(int)NumberList[nElement]);
					
					plateData[i+(nCol*nRowMax)].nNumber_Answer = (int)NumberList[nElement];
					nSum += (int)NumberList[nElement];
					
					NumberList.RemoveAt (nElement);
					
					if(i == nRow+plateData[nPos].nRowCount){
						if(nSum == plateData[nPos].nRowNumber){
							
							e (i+(nCol*nRowMax)+1);
							
						} else {
							
							e (nPos);
							
						}
						flag = true;
					}
					
				}
			}while(flag == false);
			
		}
		
	}
	
	public bool	checkCorrectAnswer(){
		
		int nRowNumber = 0, nColNumber = 0;
		for(int i = 0 ; i < nPlateMax ; i++){
			if(plateData[i].nPlateType == (int)PLATE_TYPE.HINT){
				if(plateData[i].nRowCount > 0){
					for(int j = 1 ; j <= plateData[i].nRowCount; j++){
						
						nRowNumber+=plateData[j+plateData[i].nPos].nNumber_Input;
						
					}
					if(plateData[i].nRowNumber != nRowNumber){
						
						return	false;
						
					}
					nRowNumber = 0;
				} 
				if(plateData[i].nColCount > 0){
					for(int j = 1 ; j <= plateData[i].nColCount; j++){
						
						nColNumber+=plateData[(j*nRowMax)+plateData[i].nPos].nNumber_Input;
						
					}
					if(plateData[i].nColNumber != nColNumber){
						
						return	false;
						
					}
					nColNumber = 0;
				}
			} else if(plateData[i].nPlateType == (int)PLATE_TYPE.INPUT){
				if(checkUseInputNumber (i) == false){
					
					return	false;
					
				}
			}
		}
		
		return	true;
		
	}
	
	public bool	checkUseInputNumber(int nPos){
		
		for(int i = 1 ; i < nRowMax ; i++){
			if(nPos+i>=nPlateMax)	break;
			if(plateData[nPos+i].nPlateType == (int)PLATE_TYPE.INPUT){
				if(plateData[nPos].nNumber_Input == plateData[nPos+i].nNumber_Input){
					
					return	false;
					
				}
			} else {
				
				break;
				
			}
		}
		
		for(int i = 1 ; i < nColMax ; i++){
			if(nPos+(i*nRowMax) >= nPlateMax)	break;
			if(plateData[nPos+(i*nRowMax)].nPlateType == (int)PLATE_TYPE.INPUT){
				if(plateData[nPos].nNumber_Input == plateData[nPos+(i*nRowMax)].nNumber_Input){
					
					return	false;
					
				}
			} else {
				
				break;
				
			}
		}
		
		return	true;
		
	}
	
	void	check(int nPos){
		
		int nRow 	= nPos % nRowMax;	
		int nCol 	= nPos / nColMax;
		int nBuf 	= 0x000000001;
		
		if (nPos>=nPlateMax)	return;
		
		if (plateData[nPos].nPlateType != (int)PLATE_TYPE.INPUT) {
			
			check (nPos+1);
			
		} else {
			
			ArrayList NumberList = new ArrayList ();
			NumberList = createNumberList (nPos);
			
			if (NumberList.Count == 0){
				
				plateData[nPos].nNumber_Answer = 0;
				check (nStartPos[0]);
				
			} else {
				
				int	nElement = UnityEngine.Random.Range(0, NumberList.Count);
				plateData[nPos].nNumber_Answer = (int)NumberList[nElement];
				
				for(int i = 0 ; i < nRowMax ; i++){
					//	値を設定したいマスが調べたヒントマスの範囲内か調べる
					if(checkArea (i+(nCol*nRowMax), nPos, 0)){
						
						int		nHintPos 	= (i+(nCol*nRowMax));
						int 	nTotal 		= 0;
						//	参照しているヒントマスが所持している入力マスが残り一つなら
						if(i+plateData[nHintPos].nRowCount -nRow == 1){
							//	入力されているマスの合計を求める
							for(int j = i+1 ; j < plateData[nHintPos].nRowCount+i ; j++){
								
								nTotal+= plateData[j+(nCol*nColMax)].nNumber_Answer;
								
							}
							int	ans = plateData[nHintPos].nRowNumber - nTotal;;
							
							if((plateData[nPos+1].nUseNumberFlag_op>>(ans-1) & nBuf) == 1 &&
							   checkUseNumber (nPos+1, ans)){
								
								plateData[nPos+1].nNumber_Answer = ans;
								transform.GetChild (nPos+1).FindChild ("number").GetComponent<TextMesh>().text = ans.ToString ();
								check (nPos+2);
								break;
								
							} else {
								
								check (nPos-1);
								break;
								
							}
							
						} 
						
						break;
						
					}
				}
				
			}
			
		}
		
	}
	
	//	指定した入力マスが使える値をリストで返す
	ArrayList	createNumberList(int nPos){
		
		//	参照している入力マスが使える値を取得
		ArrayList 	NumberList = new ArrayList ();
		int		 	nBuf 		= 0x000000001;
		//		plate 		plateData 	= transform.GetChild (nPos).GetComponent<plate> ();
		for(int i = 1 ; i <= 9 ; i++){
			//	使用可能な値なら配列に格納
			if((plateData[nPos].nUseNumberFlag_op>>(i-1) & nBuf)== 1){
				NumberList.Add (i);
			}
		}	
		return	NumberList;
		
	}
	
	void	createPanel(){
		
		Vector3	pos 	= Vector3.zero;
		Vector3	scale	= new Vector3(1.0f, 1.0f, 1.0f);
		int		nTate 	= 0; int	nYoko = 0;
		
		plateData = new PLATEDATA[nPlateMax];
		
		for(int i = 0 ; i < nPlateMax ; i++){
			
			//	Panel
			GameObject plateObj 				= Instantiate (platePrefab, Vector3.zero, Quaternion.identity) as GameObject;
			plateObj.transform.parent 			= GameObject.Find ("Panel").transform.FindChild ("plates").transform;
			plateObj.transform.localScale 		= scale;
			plateObj.transform.localRotation 	= Quaternion.Euler(0, 0, 0);
			
			int		nRowPos 	= i % nRowMax;
			int		nColPos 	= i / nColMax;
			int		nRowCount 	= 0, nColCount = 0;
			int		nType 		= (int)json_plateData[gameInfo.getLevel()][gameInfo.getStageType ()][i]["t"];

			if(nType == (int)PLATE_TYPE.INPUT){
				
				plateObj.name = "plate_input";
				plateObj.transform.GetChild (0).GetComponent<UISprite>().spriteName = "sgn_plate_input";
				plateObj.GetComponent<plate>().createFrame ();
				plateObj.GetComponent<plate>().createDeleteButton ();
				
			} else if(nType == (int)PLATE_TYPE.BLACK){
				
				plateObj.name = "plate_normal";
				plateObj.transform.GetChild (0).GetComponent<UISprite>().spriteName = "sgn_plate_normal";
				
			} else if(nType == (int)PLATE_TYPE.HINT){
				
				plateObj.name = "plate_hint";
				plateObj.transform.GetChild (0).GetComponent<UISprite>().spriteName = "sgn_plate_hint";
				
				// 参照しているヒントマスの右を調べ、入力マスがあれば行カウンタ増加
				for(int j = nRowPos+1 ; j < nRowMax ; j++){
					int	nCheckType = (int)json_plateData[gameInfo.getLevel ()][gameInfo.getStageType ()][j+(nColPos*nRowMax)]["t"];
					if(nCheckType == (int)PLATE_TYPE.INPUT){
						nRowCount++;
					} else {
						break;
					}
				}
				
				
				// 参照しているヒントマスの下を調べ、入力マスがあれば列カウンタ増加
				for(int j = nColPos+1 ; j < nColMax ; j++){
					int	nCheckType = (int)json_plateData[gameInfo.getLevel ()][gameInfo.getStageType ()][nRowPos+(j*nRowMax)]["t"];
					if(nCheckType == (int)PLATE_TYPE.INPUT){
						nColCount++;
					} else {
						break;
					}
				}
				
			}
			
			//			(int)json_plateData[nStageNum][i]["t"]
			//	表示用マスデータの初期化
			plateObj.GetComponent<plate>().plateData = new PLATEDATA(i, nRowPos, nColPos, nType, 0, 0, 0, 0, 
			                                                         nRowCount, nColCount, 0);
			//	作業用マスデータの初期化
			plateData[i] = plateObj.GetComponent<plate>().plateData;
			
			nYoko = i%nRowMax;
			
			int		nWidth 	= plateObj.transform.FindChild ("Background").GetComponent<UISprite>().width;
			int		nHeight = plateObj.transform.FindChild ("Background").GetComponent<UISprite>().height;
			pos = new Vector3(nYoko*nWidth, nTate*-nHeight, 0.0f);
			plateObj.transform.localPosition 		= pos;
			plateObj.GetComponent<plate>().plateData.nNumber_Answer 	= 0;
			
			if(i%nRowMax == nRowMax-1)	nTate++;
			
			if(nType == (int)PLATE_TYPE.INPUT){
				
				//	Number
				GameObject	numberObj 						= Instantiate (numberPrefab) as GameObject;
				numberObj.transform.parent					= plateObj.transform;
				numberObj.name 								= "number";
				numberObj.transform.localPosition 			= new Vector3(-1.5f, 0.0f, 3.0f);
				numberObj.transform.localRotation 			= Quaternion.Euler(0, 0, 0);
				numberObj.transform.localScale 				= new Vector3(1.0f, 1.0f, 1.0f);
				numberObj.GetComponent<UISprite>().width 	= 28;
				numberObj.GetComponent<UISprite>().height  	= 42;
				numberObj.GetComponent<UISprite>().depth 	= 6;
				numberObj.gameObject.SetActive (false);
				
			}
			
			if(plateObj.GetComponent<plate>().plateData.nRowCount > 0){
				
				Vector3[]	position = new Vector3[]{
					new Vector3(18, 15, 0), new Vector3(6, 15, 0)
				};
				for(int j = 0 ; j < 2 ; j++){
					createHintNumber (i, position[j], 0, "hintNumber_row_0"+j.ToString ());
				}

			}
			if(plateObj.GetComponent<plate>().plateData.nColCount > 0){

				Vector3[]	position = new Vector3[]{
					new Vector3(-6, -15, 0), new Vector3(-18, -15, 0)
				};
				for(int j = 0 ; j < 2 ; j++){
					createHintNumber (i, position[j], 0, "hintNumber_col_0"+j.ToString ());
				}

			}
			
		}
		
	}

	public int	getInputNumberSum(int nHintPos, int nAreaType){

		if (nHintPos != plateData [nHintPos].nPos)	return	0;
	
		int nSum = 0;

		if(nAreaType == (int)AREA.ROW){
			for(int j = 1 ; j <= plateData[nHintPos].nRowCount; j++){
				nSum+=plateData[j+plateData[nHintPos].nPos].nNumber_Input;
			}
		} 

		if(nAreaType == (int)AREA.COL){
			for(int j = 1 ; j <= plateData[nHintPos].nColCount; j++){
				nSum+=plateData[(j*nRowMax)+plateData[nHintPos].nPos].nNumber_Input;
			}
		}
		return	nSum;

	}

	void	createHintNumber(int nPos, Vector3 position, int nNumber, string name){
		
		GameObject	numberObj 						= Instantiate (numberPrefab) as GameObject;
		numberObj.transform.parent					= GameObject.Find ("Panel").transform.FindChild ("plates").GetChild (nPos).transform;
		numberObj.name 								= name;
		numberObj.transform.localPosition 			= position;
		numberObj.transform.localRotation 			= Quaternion.Euler(0, 0, 0);
		numberObj.transform.localScale 				= new Vector3 (1.0f, 1.0f, 1.0f);
		numberObj.GetComponent<UISprite>().width 	= 16;
		numberObj.GetComponent<UISprite>().height	= 20;
		numberObj.gameObject.SetActive (false);
		
	}
	
	bool	createAnswer(int nPos){
		
		int		row 		= nPos / nRowMax;
		int		col 		= nPos % nColMax;
		int 	nRand 		= 0;
		int 	nFlag 		= 0x000000000;
		int		nBuf 		= 0x000000001;
		double	nAll 		= 0x111111111;
		bool	flag 		= false;
		int nCount = 0;
		
		for (int i = 0; i < 10; i++) {
			if(plateData[row*10+i].nPlateType == (int)PLATE_TYPE.INPUT){
				
				int		nNum = plateData[row*10+i].nNumber_Answer;
				if(nNum != 0){
					nFlag |= (nBuf<<(nNum-1));
					nBuf = 0x000000001;
				}
				if(nFlag == nAll)	return	false;
				
			}
			if(plateData[col+i*10].nPlateType == (int)PLATE_TYPE.INPUT){
				
				int		nNum = plateData[col+i*10].nNumber_Answer;
				if(nNum != 0){
					nFlag |= (nBuf<<(nNum-1));
					nBuf = 0x000000001;
				}
				if(nFlag == nAll)	return	false;
				
			}
			
		}
		
		do{
			
			nCount++;
			nRand = UnityEngine.Random.Range (1, 9+1);
			if((nFlag & (nBuf<<nRand-1)) == 0)	flag = true;	
			if(nCount >= 2000)	return	false;
			
		}while(flag == false);
		
		plateData[nPos].nNumber_Answer = nRand;
		
		return	true;
		
	}
	
	bool	hintCheck(int nPos){
		
		for(int i = 1 ; i <= nColMax - plateData[nPos].nColPos ; i++){
			if(nPos+(i*nColMax) >= nPlateMax)	break;
			if(plateData[nPos+(i*nColMax)].nPlateType == (int)PLATE_TYPE.HINT){
				//	ヒントマスが下に並んでいて、その入力マス数と合計数が一致するか確認する
				if(plateData[nPos+(i*nColMax)].nRowNumber == plateData[nPos].nRowNumber){
					if(plateData[nPos+(i*nColMax)].nRowCount == plateData[nPos].nRowCount){
						
						return	false;
						
					}
				}
				//	入力マスか黒マスにぶつかったらチェック終了
			} else if(plateData[nPos+(i*nColMax)].nPlateType == (int)PLATE_TYPE.BLACK ||
			          plateData[nPos+(i*nColMax)].nPlateType == (int)PLATE_TYPE.INPUT){
				
				break;
				
			}
			
		}
		
		for(int i = 1 ; i <= nRowMax - plateData[nPos].nRowPos ; i++){
			if(nPos+i >= nPlateMax)	break;
			if(plateData[nPos+i].nPlateType == (int)PLATE_TYPE.HINT){
				//	ヒントマスが右に並んでいて、その入力マス数と合計数が一致するか確認する
				if(plateData[nPos+i].nColNumber == plateData[nPos].nColNumber){
					if(plateData[nPos+i].nColCount == plateData[nPos].nColCount){
						
						return	false;
						
					}
				}
				//	入力マスか黒マスにぶつかったらチェック終了
			} else if(plateData[nPos+i].nPlateType == (int)PLATE_TYPE.BLACK ||
			          plateData[nPos+i].nPlateType == (int)PLATE_TYPE.INPUT){
				
				break;
				
			}
			
		}
		
		return	true;
		
	}
	/*
	void	createHints(int nPos){

		if(transform.GetChild (nPos).GetComponent<plate>().nRowCount != 0){
		
			int		nNumber = hintNumber (transform.GetChild (nPos).GetComponent<plate>().nRowCount);
			transform.GetChild (nPos).GetComponent<plate>().nRowNumber = nNumber;
			transform.GetChild (nPos).FindChild ("hintNumber_row").GetComponent<TextMesh>().text = nNumber.ToString ();

		}
		
		if(transform.GetChild (nPos).GetComponent<plate>().nColCount != 0){
			
			int		nNumber = hintNumber (transform.GetChild (nPos).GetComponent<plate>().nColCount);
			transform.GetChild (nPos).GetComponent<plate>().nColNumber = nNumber;
			transform.GetChild (nPos).FindChild ("hintNumber_col").GetComponent<TextMesh>().text = nNumber.ToString ();
			
		}

	}
*/
	
	int	setHintNumber(int nCount){
		
		int 	nNumber = 0;
		int 	nFlag 	= 0x000000000;
		int 	nLoop 	= 0;
		int		nBuf 	= 0x000000001;
		int[][] nHintData = new int[][]{
			
			new int[]{ 3, 4, 5, 6, 15, 16, 17 },				//	2マス
			new int[]{ 6, 7, 8, 22, 23, 24 },					//	3マス
			new int[]{ 10, 11, 28, 29, 30 },					//	4マス
			new int[]{ 15, 16, 17, 18, 32, 33, 34, 35 },		//	5マス
			new int[]{ 21, 22, 23, 24, 36, 37, 38, 39 },		//	6マス
			new int[]{ 28, 29, 30, 41, 42 },					//	7マス
			new int[]{ 36, 37, 38, 39, 40, 41, 42, 43, 44 },	//	8マス
			new int[]{ 45 }										//	9マス
			
		};
		bool 	fCreate = false;
		
		int rnd = UnityEngine.Random.Range (0, 100);
		if(rnd >= 0){


			int	n = UnityEngine.Random.Range (0, nHintData[nCount-2].Length);
			return	nHintData[nCount-2][n];
			
		} else {
			do{
				
				int	nRand = UnityEngine.Random.Range (1, 9+1);
				if(((nFlag & (nBuf<<nRand))>>nRand-1) == 0){
					
					nFlag |= nBuf<<nRand;
					nNumber += nRand;
					nLoop++;
					
				}
				
				if(nLoop == nCount)	fCreate = true;
				
			} while(fCreate == false);
		}
		
		return	nNumber;
		
	}
	
	void	a(int nPos){
		
		transform.GetChild (nPos).GetComponent<plate> ().plateData.nRowNumber = 0;
		transform.GetChild (nPos).GetComponent<plate> ().plateData.nColNumber = 0;
		plateData [nPos].nRowNumber = 0;
		plateData [nPos].nColNumber = 0;
		//	前回のヒントデータ削除
		if(transform.GetChild (nPos).childCount > 0){
			for(int i = 0 ; i < transform.GetChild (nPos).childCount ; i++){
				
				Destroy(transform.GetChild (nPos).GetChild (i).gameObject);
				
			}
		}
		
		//	右にあるマスを調べ、白マスが何個あるか調べる
		//	右にあるマスの個数分ループ
		for(int i = 0 ; i < nRowMax - (plateData[nPos].nRowPos+1) ; i++){
			
			//	黒いマスにぶつかるか、右端まで調べたら終了
			if(plateData[nPos+(i+1)].nPlateType == (int)PLATE_TYPE.BLACK || plateData[nPos+(i+1)].nPlateType == (int)PLATE_TYPE.HINT ||
			   ((plateData[nPos].nRowPos+1)+i) == nRowMax - 1){
				if(transform.GetChild (nPos).GetComponent<plate>().plateData.nRowCount != 0){
					
					Vector3	position = new Vector3(1.0f, 0.0f, 4.0f);
					createHintNumber (nPos, position, plateData[nPos].nRowNumber, "hintNumber_row");
					transform.GetChild (nPos).name = "plate_Hint";
					
				}
				
				break;
				
			} else {
				
				plateData[nPos].nRowNumber+=plateData[nPos+(i+1)].nNumber_Answer;
				
			}
		}
		
		//	下にあるマスを調べ、白マスが何個あるか調べる
		//	下にあるマスの個数分ループ
		for(int i = 0 ; i < nColMax - (plateData[nPos].nColPos+1) ; i++){
			//	黒いマスにぶつかるか、下端まで調べたら終了
			if(plateData[nPos+((i+1)*nColMax)].nPlateType == (int)PLATE_TYPE.BLACK || plateData[nPos+((i+1)*nColMax)].nPlateType == (int)PLATE_TYPE.HINT ||
			   ((plateData[nPos].nColPos+1)+i) == nColMax - 1){
				if(plateData[nPos].nColCount != 0){
					
					Vector3	position = new Vector3(-4.0f, 0.0f, -1.0f);	
					createHintNumber (nPos, position, plateData[nPos].nColNumber, "hintNumber_col");
					transform.GetChild (nPos).name = "plate_Hint";
					
				}
				
				break;
				
			} else {
				
				plateData[nPos].nColNumber+=plateData[nPos+((i+1)*nColMax)].nNumber_Answer;
				
			}
		}
		
	}
	
	int	getRowFlag(int nPos){
		
		int[]		nRowNum 	= new int[9];
		int 		nRowFlag 	= 0x000000000;
		String 		rowKey 		= plateData[nPos].nRowNumber + "," + plateData[nPos].nRowCount;
		
		for(int i = 8 ; i >= 0 ; i--){
			
			nRowNum[i] = (int)json_useNumber[rowKey][i];
			if(nRowNum[i] == 1){
				
				int		nBuf = 0x000000001;
				nRowFlag |= nBuf<<(9-i)-1;
				
			}
			
		}
		
		return	nRowFlag;
		
	}
	
	int	getColFlag(int nPos){
		
		int[]	nColNum 	= new int[9];
		int 	nColFlag 	= 0x000000000;
		String colKey = plateData [nPos].nColNumber + "," + plateData [nPos].nColNumber;
		
		for(int i = 8 ; i >= 8 ; i++){
			
			nColNum[i] = (int)json_useNumber[colKey][i];
			if(nColNum[i] == 1){
				
				int		nBuf = 0x000000001;
				nColFlag |= nBuf<<(9-i)-1;
				
			}
			
		}
		
		return	nColFlag;
		
	}
	
	int	canbePlaced(int nRowCount, int nRowSum, int nColCount, int nColSum){
		
		int[]		nRowNum 	= new int[9];
		int[]		nColNum 	= new int[9];
		int 		nRowFlag 	= 0x000000000;
		int 		nColFlag 	= 0x000000000;
		String 		rowKey 		= nRowSum + "," + nRowCount;
		String 		colKey 		= nColSum + "," + nColCount;
		
		for(int i = 8 ; i >= 0 ; i--){
			
			nRowNum[i] = (int)json_useNumber[rowKey][i];
			if(nRowNum[i] == 1){
				
				int		nBuf = 0x000000001;
				nRowFlag |= nBuf<<(9-i)-1;
				
			}
			
		}
		for(int i = 8 ; i >= 0 ; i--){
			
			nColNum[i] = (int)json_useNumber[colKey][i];
			if(nColNum[i] == 1){
				
				int		nBuf = 0x000000001;
				nColFlag |= nBuf<<(9-i)-1;           
				
			}
			
		}
		
		return	nRowFlag & nColFlag;
		
	}
	
	bool	checkUseNumber(int nPos, int nNumber){
		
		int		nSetNumber 	= 0;
		int		nBuf 	= 0x000000001;
		int 	nRow 	= nPos % nRowMax;	
		int 	nCol 	= nPos / nColMax;
		int 	nStart 	= 0;
		int 	nEnd 	= 0;
		
		while(true){
			
			if(nNumber == 0){
				
				//	乱数取得
				nSetNumber = UnityEngine.Random.Range (1, 9+1);
				//	使用できない値なら乱数取得からやり直す
				if((plateData[nPos].nUseNumberFlag_op>>(nSetNumber-1) & nBuf) == 0){
					
					continue;
					
				}
				
			} else {
				
				nSetNumber = nNumber;
				
			}
			
			/////////////////////////////////////////////////////////////////////
			//	行検索
			/////////////////////////////////////////////////////////////////////
			//	値を設定したいマスがある行を調べる
			for(int i = 0 ; i < nRowMax ; i++){
				//	ヒントマスかどうか
				if(plateData[i+(nCol*nRowMax)].nPlateType == (int)PLATE_TYPE.HINT){
					//	値を設定したいマスが調べたヒントマスの範囲内か調べる
					if(checkArea (i+(nCol*nRowMax), nPos, 0)){
						
						nStart 	= i+1;								//	ループ最小値
						nEnd 	= (nStart+plateData[i+(nCol*nRowMax)].nRowCount)-1;	//	ループ最大値
						
						//Debug.Log ("nPos:"+nPos + "     nStart:"+nStart + "    nEnd:" + nEnd);
						
						//	調べたヒントマスの範囲内にある入力マス分ループ
						for(int j = nStart ; j <= nEnd ; j++){
							//	設定したい値が既に他の入力マスに設定されている値なら
							if(plateData[j+(nCol*nRowMax)].nNumber_Answer == nSetNumber){
								if(nNumber != 0)	return false;
								
								//	使用可能状態なら使用不可にする
								if(((plateData[nPos].nUseNumberFlag_op>>(plateData[j+(nCol*nRowMax)].nNumber_Answer-1)) & nBuf ) == 1){
									
									plateData[nPos].nUseNumberFlag_op ^= (nBuf<<(nSetNumber-1));
									nSetNumber = 0;
									
								}
								//	全ての値が入力不可ならこの関数を抜ける
								if(plateData[nPos].nUseNumberFlag_op == 0x000000000){
									
									return	false;
									
								}
								break;
							}
							
						}
						
					}
				}
				
			}
			
			if(nSetNumber == 0)	continue;
			
			/////////////////////////////////////////////////////////////////////
			//	列検索
			/////////////////////////////////////////////////////////////////////
			//	値を設定したいマスがある列を調べる
			for(int i = 0 ; i < nColMax ; i++){
				//Debug.Log ("pos:"+(nCol+(i*nRowMax)) + "     type:"+plate_col.nPlateType + "     nSetNumber:"+nSetNumber);
				//	ヒントマスかどうか
				if(plateData[nRow+(i*nRowMax)].nPlateType == (int)PLATE_TYPE.HINT){
					//	値を設定したいマスが調べたヒントマスの範囲内か調べる
					if(checkArea (nRow+(i*nRowMax), nPos, 1)){
						
						nStart 	= i+1;								//	ループ最小値
						nEnd 	= (nStart+plateData[nRow+(i*nRowMax)].nColCount)-1;	//	ループ最大値
						
						//Debug.Log ("nPos:"+nPos + "     nStart:"+nStart + "    nEnd:" + nEnd);
						
						//	調べたヒントマスの範囲内にある入力マス分ループ
						for(int j = nStart ; j <= nEnd ; j++){
							//	設定したい値が既に他の入力マスに設定されている値なら
							if(plateData[nRow+(j*nRowMax)].nNumber_Answer == nSetNumber){
								if(nNumber != 0)	return false;
								//	使用可能状態なら使用不可にする
								if(((plateData[nPos].nUseNumberFlag_op>>(plateData[nRow+(j*nRowMax)].nNumber_Answer-1)) & nBuf ) == 1){
									
									plateData[nPos].nUseNumberFlag_op ^= (nBuf<<(nSetNumber-1));
									nSetNumber = 0;
									
								}
								//	全ての値が入力不可ならこの関数を抜ける
								if(plateData[nPos].nUseNumberFlag_op == 0x000000000){
									return	false;
								}
								break;
							}
							//	調べ終わったらループを終了する
							if(j == nEnd){
								
								//Debug.Log ("nPos:"+nPos + "    nNumber:"+nSetNumber);
								plateData[nPos].nNumber_Answer = nSetNumber;
								
								return	true;
								
							}
							
						}
						
					}	
				}
			}
			
		};
		
	}
	
	bool	checkArea(int nHintPos, int nCheckPos, int nDirection){
		
		int 	nStart 	= 0;	//	ループ最小値
		int 	nEnd 	= 0;	//	ループ最大値
		
		if(plateData[nHintPos].nPlateType != (int)PLATE_TYPE.HINT){
			
			return	false;
			
		}
		
		if(nDirection == 0){
			if(plateData[nHintPos].nRowCount == 0){
				
				return	false;
				
			}
			
			int 	nCheckRow 		= nCheckPos % nRowMax;	
			int 	nHintRow 		= nHintPos % nRowMax;	
			
			nStart 	= nHintRow + 1;
			nEnd 	= (nStart + plateData[nHintPos].nRowCount) - 1;
			
			if(nStart <= nCheckRow && nEnd >= nCheckRow){
				
				return	false;
				
			} else {
				
				return	false;
				
			}
			
		} else if(nDirection == 1){
			if(plateData[nHintPos].nColCount == 0){
				
				return	false;
				
			}
			
			int 	nCheckCol 		= nCheckPos / nColMax;
			int 	nHintCol 		= nHintPos / nColMax;
			
			nStart 	= nHintCol + 1;
			nEnd 	= (nStart + plateData[nHintPos].nColCount) - 1;
			
			if(nStart <= nCheckCol && nEnd >= nCheckCol){
				
				return	true;
				
			} else {
				
				return	false;
				
			}
			
		}
		
		return	true;
		
	}
	
	void	checkInputPlateNumber(int nPos){
		
		int nRow = nPos % nRowMax;
		
		//	右にあるマスの個数分ループ
		for(int i = 0 ; i < plateData[nPos].nRowCount ; i++){
			if(plateData[nPos+(i+1)].nPlateType == (int)PLATE_TYPE.INPUT){
				for(int j = 0 ; j < nColMax ; j++){				
					if(plateData[(nRow+1+i)+(j*nColMax)].nPlateType == (int)PLATE_TYPE.HINT && plateData[(nRow+1+i)+(j*nColMax)].nColCount > 0){
						
						//	横のヒントマスと縦のヒントマス両方に共通する数字を調べる
						plateData[nPos+(i+1)].nUseNumberFlag = canbePlaced (plateData[nPos].nRowCount, plateData[nPos].nRowNumber, 
						                                                    plateData[(nRow+1+i)+(j*nColMax)].nColCount, plateData[(nRow+1+i)+(j*nColMax)].nColNumber);
						plateData[nPos+(i+1)].nUseNumberFlag_op = plateData[nPos+(i+1)].nUseNumberFlag;
						break;
						
					}
				}
			} else {
				
				return;
				
			}		
		}
		
	}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*	void Update () {

//		fCheckComplate = false;
		for(int i = 0 ; i < nPlateMax ; i++){

			GameObject.Find ("UI Root/Panel/plates").transform.GetChild (i).GetComponent<plate>().Init ();
			plateData[i] = GameObject.Find ("UI Root/Panel/plates").transform.GetChild (i).GetComponent<plate>().plateData;

			
		}

		if(fCheckComplate == false){
			if(PlayerPrefs.GetInt ("fContinue") == 1){

				gameObject.AddComponent("saveLoad").GetComponent<saveLoad>().loadStageData ();
				gameInfo.setContinueFlag (false);
				PlayerPrefs.SetInt ("fContinue", 0);
				for(int i = 0 ; i < nPlateMax ; i++){
					if(plateData[i].nPlateType == (int)PLATE_TYPE.INPUT){
						if(plateData[i].nNumber_Input > 0){

							plate	plateObj 	= GameObject.Find ("UI Root/Panel/plates").transform.GetChild (i).GetComponent<plate>();
							plateObj.plateData 	= plateData[i];
							plateObj.plateData.fInput = true;
							plateObj.transform.FindChild ("number").GetComponent<UISprite>().spriteName = "info_num_0"+plateObj.plateData.nNumber_Answer.ToString ()+"w";

						}
					}
				}

			} else {

				createKakkuro ();

			}
			GameObject.Find ("Panel").GetComponent<panel>().fTimerStart = true;
		
		}
		fCheckComplate = true;
			
		for(int i = 0 ; i < nPlateMax ; i++){


			plate	plateObj 	= GameObject.Find ("UI Root/Panel/plates").transform.GetChild (i).GetComponent<plate>();
			plateObj.plateData 	= plateData[i];

			if(plateObj.plateData.nPlateType == (int)PLATE_TYPE.INPUT){

		//		plateObj.transform.FindChild ("number").gameObject.SetActive (true);
		//		Debug.Log ("Pos:"+i + "    Answer:"+plateObj.plateData.nNumber_Answer.ToString ());
				plateObj.transform.FindChild ("number").gameObject.SetActive (false);
				plateObj.transform.FindChild ("number").GetComponent<UISprite>().spriteName = "info_num_0"+plateObj.plateData.nNumber_Answer.ToString ();

			}

			if(plateObj.plateData.nPlateType == (int)PLATE_TYPE.HINT){

				plateObj.transform.FindChild("Background").GetComponent<UISprite>().spriteName = "sgn_plate_hint";

			}
		
			if(plateObj.plateData.nRowCount > 0){
				if(plateObj.plateData.nRowNumber <= 9){

					plateObj.transform.FindChild ("hintNumber_row_00").gameObject.SetActive(true);
					plateObj.transform.FindChild ("hintNumber_row_00").GetComponent<UISprite>().spriteName = "info_num_0"+plateObj.plateData.nRowNumber.ToString ()+"w";
					plateObj.transform.FindChild ("hintNumber_row_01").gameObject.SetActive(false);

				} else {
					int	nNum = 0;
					//	１の位を求める
					plateObj.transform.FindChild ("hintNumber_row_00").gameObject.SetActive(true);
					nNum = plateObj.plateData.nRowNumber % 10;
					plateObj.transform.FindChild ("hintNumber_row_00").GetComponent<UISprite>().spriteName = "info_num_0"+nNum.ToString ()+"w";
					//	１０の位を求める
					plateObj.transform.FindChild ("hintNumber_row_01").gameObject.SetActive(true);
					nNum = plateObj.plateData.nRowNumber / 10;
					plateObj.transform.FindChild ("hintNumber_row_01").GetComponent<UISprite>().spriteName = "info_num_0"+nNum.ToString ()+"w";

				}
			}

			if(plateObj.plateData.nColCount > 0){
				if(plateObj.plateData.nColNumber <= 9){

					plateObj.transform.FindChild ("hintNumber_col_00").gameObject.SetActive (true);						
					plateObj.transform.FindChild ("hintNumber_col_00").GetComponent<UISprite>().spriteName = "info_num_0"+plateObj.plateData.nColNumber.ToString ()+"w";
					plateObj.transform.FindChild ("hintNumber_col_01").gameObject.SetActive(false);
					
				} else {

					int	nNum = 0;
					//	１の位を求める
					plateObj.transform.FindChild ("hintNumber_col_00").gameObject.SetActive(true);
					nNum = plateObj.plateData.nColNumber % 10;
					plateObj.transform.FindChild ("hintNumber_col_00").GetComponent<UISprite>().spriteName = "info_num_0"+nNum.ToString ()+"w";
					//	１０の位を求める
					plateObj.transform.FindChild ("hintNumber_col_01").gameObject.SetActive(true);
					nNum = plateObj.plateData.nColNumber / 10;
					plateObj.transform.FindChild ("hintNumber_col_01").GetComponent<UISprite>().spriteName = "info_num_0"+nNum.ToString ()+"w";
					
				}
			}
	
		}

	}

	/// <summary>
	///	正解判定を行う
	/// </summary>
	public bool	checkCorrectAnswer(){

		int nRowNumber = 0, nColNumber = 0;
		for(int i = 0 ; i < nPlateMax ; i++){
			if(plateData[i].nPlateType == (int)PLATE_TYPE.HINT){
				if(plateData[i].nRowCount > 0){
					for(int j = 1 ; j <= plateData[i].nRowCount; j++){

						nRowNumber+=plateData[j+plateData[i].nPos].nNumber_Input;

					}
					if(plateData[i].nRowNumber != nRowNumber){
						
						return	false;
						
					}
					nRowNumber = 0;
				} 
				if(plateData[i].nColCount > 0){
					for(int j = 1 ; j <= plateData[i].nColCount; j++){
						
						nColNumber+=plateData[(j*nRowMax)+plateData[i].nPos].nNumber_Input;
						
					}
					if(plateData[i].nColNumber != nColNumber){
						
						return	false;
						
					}
					nColNumber = 0;
				}
			} else if(plateData[i].nPlateType == (int)PLATE_TYPE.INPUT){
				if(checkUseInputNumber (i) == false){

					return	false;

				}
			}
		}

		return	true;

	}

	/// <summary>
	///	入力した数字のチェックを行う
	/// </summary>
	public bool	checkUseInputNumber(int nPos){

		for(int i = 1 ; i < nRowMax ; i++){
			if(plateData[nPos+i].nPlateType == (int)PLATE_TYPE.INPUT){
				if(plateData[nPos].nNumber_Input == plateData[nPos+i].nNumber_Input){

					return	false;

				}
			} else {

				break;

			}
		}

		for(int i = 1 ; i < nColMax ; i++){
			if(plateData[nPos+(i*nRowMax)].nPlateType == (int)PLATE_TYPE.INPUT){
				if(plateData[nPos].nNumber_Input == plateData[nPos+(i*nRowMax)].nNumber_Input){
					
					return	false;
					
				}
			} else {
				
				break;
				
			}
		}

		return	true;

	}

	/// <summary>
	///	指定した入力マスが使える値をリストで返す
	/// </summary>
	ArrayList	createNumberList(int nPos){

		//	参照している入力マスが使える値を取得
		ArrayList 	NumberList = new ArrayList ();
		int		 	nBuf 		= 0x000000001;

		for(int i = 1 ; i <= 9 ; i++){
			//	使用可能な値なら配列に格納
			if((plateData[nPos].nUseNumberFlag_op>>(i-1) & nBuf)== 1){
				
				NumberList.Add (i);
				
			}
		}

		return	NumberList;

	}
	
	void	createPanel(){

		Vector3	pos 	= Vector3.zero;
		Vector3	scale	= new Vector3(1.0f, 1.0f, 1.0f);
		int		nTate 	= 0; int	nYoko = 0;

		plateData = new PLATEDATA[nPlateMax];

		for(int i = 0 ; i < nPlateMax ; i++){

			//	Panel
			GameObject plateObj 				= Instantiate (platePrefab, Vector3.zero, Quaternion.identity) as GameObject;
			plateObj.transform.parent 			= GameObject.Find ("UI Root/Panel/plates").transform;
			plateObj.transform.localScale 		= scale;
			plateObj.transform.localRotation 	= Quaternion.Euler(0, 0, 0);

			int		nRowPos 	= i % nRowMax;
			int		nColPos 	= i / nColMax;
			int		nRowCount 	= 0, nColCount = 0;
			int		nType 		= (int)json_plateData[gameInfo.getLevel()][gameInfo.getStageType()][i]["t"];

			if(nType == (int)PLATE_TYPE.INPUT){
				
				plateObj.name = "plate_input";
				plateObj.transform.GetChild (0).GetComponent<UISprite>().spriteName = "sgn_plate_input";
				
			} else if(nType == (int)PLATE_TYPE.BLACK){
				
				plateObj.name = "plate_normal";
				plateObj.transform.GetChild (0).GetComponent<UISprite>().spriteName = "sgn_plate_normal";
				
			} else if(nType == (int)PLATE_TYPE.HINT){
				
				plateObj.name = "plate_hint";
				plateObj.transform.GetChild (0).GetComponent<UISprite>().spriteName = "sgn_plate_hint3";

				// 参照しているヒントマスの右を調べ、入力マスがあれば行カウンタ増加
				for(int j = nRowPos+1 ; j < nRowMax ; j++){
					int	nCheckType = (int)json_plateData[gameInfo.getLevel()][gameInfo.getStageType ()][j+(nColPos*nRowMax)]["t"];
					if(nCheckType == (int)PLATE_TYPE.INPUT){
						nRowCount++;
					} else {
						break;
					}
				}

				// 参照しているヒントマスの下を調べ、入力マスがあれば列カウンタ増加
				for(int j = nColPos+1 ; j < nColMax ; j++){
					int	nCheckType = (int)json_plateData[gameInfo.getLevel()][gameInfo.getStageType ()][nRowPos+(j*nRowMax)]["t"];
					if(nCheckType == (int)PLATE_TYPE.INPUT){
						nColCount++;
					} else {
						break;
					}
				}
			
			}
		
			//	表示用マスデータの初期化
			plateObj.GetComponent<plate>().plateData = new PLATEDATA(i, nRowPos, nColPos, nType, 0, 0, 0, 0, 
			                                                         nRowCount, nColCount, 0);
			//	作業用マスデータの初期化
			plateData[i] = plateObj.GetComponent<plate>().plateData;

			nYoko = i%nRowMax;

			int		nWidth 	= plateObj.transform.FindChild ("Background").GetComponent<UISprite>().width;
			int		nHeight = plateObj.transform.FindChild ("Background").GetComponent<UISprite>().height;
			pos = new Vector3(nYoko*nWidth, nTate*-nHeight, 0.0f);
			plateObj.transform.localPosition 		= pos;
			plateObj.GetComponent<plate>().plateData.nNumber_Answer 	= 0;

			if(i%nRowMax == nRowMax-1)	nTate++;

			if(nType == (int)PLATE_TYPE.INPUT){

				//	Number
				GameObject	numberObj 						= Instantiate (numberPrefab) as GameObject;
				numberObj.transform.parent					= plateObj.transform;
				numberObj.name 								= "number";
				numberObj.transform.localPosition 			= new Vector3(-1.5f, 0.0f, 3.0f);
				numberObj.transform.localRotation 			= Quaternion.Euler(0, 0, 0);
				numberObj.transform.localScale 				= new Vector3(1.0f, 1.0f, 1.0f);
				numberObj.GetComponent<UISprite>().width 	= 28;
				numberObj.GetComponent<UISprite>().height  	= 42;
				numberObj.gameObject.SetActive (false);

			}

			if(plateObj.GetComponent<plate>().plateData.nRowCount > 0){

				Vector3[]	position = new Vector3[]{

					new Vector3(18, 15, 0), new Vector3(6, 15, 0)

				};

				for(int j = 0 ; j < 2 ; j++){

					createHintNumber (i, position[j], 0, "hintNumber_row_0"+j.ToString ());

				}

			}
			if(plateObj.GetComponent<plate>().plateData.nColCount > 0){

				Vector3[]	position = new Vector3[]{
					
					new Vector3(-6, -15, 0), new Vector3(-18, -15, 0)
					
				};

				for(int j = 0 ; j < 2 ; j++){

					createHintNumber (i, position[j], 0, "hintNumber_col_0"+j.ToString ());
					
				}
			}

		}

	}

	void	createHintNumber(int nPos, Vector3 position, int nNumber, string name){

		GameObject	numberObj 						= Instantiate (numberPrefab) as GameObject;
		numberObj.transform.parent				 	= GameObject.Find ("UI Root/Panel/plates").transform.GetChild (nPos).transform;
		numberObj.name 								= name;
		numberObj.transform.localPosition 			= position;
		numberObj.transform.localRotation 			= Quaternion.Euler(0, 0, 0);
		numberObj.transform.localScale 				= new Vector3 (1.0f, 1.0f, 1.0f);
		numberObj.GetComponent<UISprite>().width 	= 16;
		numberObj.GetComponent<UISprite>().height	= 20;
		numberObj.gameObject.SetActive (false);

	
	}
	
	int	setHintNumber(int nPos, int nDirection){

		int 		nFlag 		= 0x000000000;			//	設定したい数字のフラグ取得用
		ArrayList 	numberList 	= new ArrayList ();		//	設定可能数字取得用
		int[][] 	nHintData_0 = new int[][]{
			
			new int[]{ 3,  4,  5, 15, 16, 17},				//	2マス
			new int[]{ 6,  7,  8, 22, 23, 24},				//	3マス
			new int[]{10, 11, 12, 28, 29, 30},				//	4マス
			new int[]{15, 16, 17, 33, 34, 35},				//	5マス
			new int[]{21, 22, 23, 37, 38, 39},				//	6マス
			new int[]{28, 29, 30, 40, 41, 42},				//	7マス
			new int[]{36, 37, 38, 39, 40, 41, 42, 43, 44},	//	8マス
			new int[]{45}									//	9マス
			
		};

		int[][] nHintData_1 = new int[][]{

			new int[]{ 6,  7,  8,  9, 10, 11, 12, 13, 14},							//	2マス
			new int[]{ 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21},			//	3マス
			new int[]{13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27},	//	4マス
			new int[]{18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32}, 	//	5マス
			new int[]{24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36},			//	6マス
			new int[]{31, 32, 33, 34, 35, 36, 37, 38, 39}							//	7マス

		};
		int		nLoopEnd_0 		= 0, nLoopEnd_1 = 0, nLoopEnd_2 = 0;
		int 	nIndex 			= 0;
		int 	nCount 			= 0;
		int 	nIndex_plate 	= 0;

		if(nDirection == (int)AREA.ROW){

			nLoopEnd_0 	= nHintData_0[plateData[nPos].nRowCount-2].Length;
			nLoopEnd_2 	= nHintData_1[plateData[nPos].nRowCount-2].Length;
			nIndex 		= plateData[nPos].nRowCount-2;
			nCount 		= plateData[nPos].nRowCount;

		} else {

			nLoopEnd_0 	= nHintData_0[plateData[nPos].nColCount-2].Length;
			nLoopEnd_2 	= nHintData_1[plateData[nPos].nColCount-2].Length;
			nIndex 		= plateData[nPos].nColCount-2;
			nCount 		= plateData[nPos].nColCount;

		}
		nLoopEnd_1 	= nCount;

		ArrayList	nHintArray = new ArrayList();
		for(int i = 0 ; i < nLoopEnd_0 ; i++){
			
			nHintArray.Add ((int)nHintData_0[nIndex][i]);
			
		}

//		print ("-----hintPos-----:"+nPos);
		for(int i = 0 ; i < nLoopEnd_0 ; i++){

			//	使用可能フラグ取得
			int		a 		= nHintArray.IndexOf (nHintData_0[nIndex][i]);
			String	key 	= nHintArray[a] + "," + nCount;
			nFlag 			= getUseNumberFlag (key);

			for(int j = 1 ; j <= nLoopEnd_1 ; j++){
				if(nDirection == (int)AREA.ROW){
					nIndex_plate = nPos+j;
				} else {
					nIndex_plate = nPos+(j*nRowMax);
				}

				//	数字が確定していないか
				if(plateData[nIndex_plate].fDefineNumber == false){

					int	nFlag_plate = plateData[nIndex_plate].nUseNumberFlag_op;
//					print ("入力可能フラグ");
//					Debug.Log ("Pos:"+nIndex_plate + "   Flag:"+Convert.ToString(nFlag_plate, 2).PadLeft (9, '0'));
					if(nFlag_plate == 0x000000000){
						nFlag_plate |= nFlag;
					} else {
						nFlag_plate &= nFlag;
					}
//					print ("入力したい値:"+nHintData_0[nIndex][i]);
//					print ("演算結果:"+Convert.ToString(nFlag_plate, 2).PadLeft (9, '0'));

					//	入力可能な値のカウンタ取得
					int	nDefineNumberCount = getDefineNumberCount (nFlag_plate);
					if(nDefineNumberCount >= 1){
						if(checkUseHintNumber (nPos, (int)nHintArray[a], nDirection) == false){

							nHintArray.RemoveAt (a);
							break;

						}
					}else if(nDefineNumberCount == 0) {

							int	n = nHintArray.IndexOf (nHintArray[a]);
							if(n > -1)	nHintArray.RemoveAt (n);

							break;

					}

				}

			}

		}

		if(nHintArray.Count > 0){

			int nRand = UnityEngine.Random.Range (0, nHintArray.Count);
//			Debug.Log ("Pos:"+nPos + "   Count:"+numberList.Count + "   Num:"+(int)numberList[nRand]);
			for(int j = 1 ; j <= nLoopEnd_1 ; j++){	
				if(nDirection == (int)AREA.ROW){
					nIndex_plate = nPos+j;
				} else {
					nIndex_plate = nPos+(j*nRowMax);
				}

				String	key 	= (int)nHintArray[nRand] + "," + nCount;
				nFlag 			= getUseNumberFlag (key);
				if(plateData[nIndex_plate].nUseNumberFlag_op == 0x000000000){	
					plateData[nIndex_plate].nUseNumberFlag_op |= nFlag;
//					print (Convert.ToString (plateData[nIndex_plate].nUseNumberFlag_op, 2).PadLeft (9, '0'));
				} else {
					plateData[nIndex_plate].nUseNumberFlag_op &= nFlag;
				}

			}

			return	(int)nHintArray[nRand];

		} else {

//			print ("確定数字がありませんでした");
			numberList.Clear ();
			nHintArray.Clear ();

			for(int i = 0 ; i < nLoopEnd_2 ; i++){
				
				nHintArray.Add ((int)nHintData_1[nIndex][i]);
				
			}

			for(int i = 0 ; i < nLoopEnd_2 ; i++){
				
				//	使用可能フラグ取得
				int		a 		= nHintArray.IndexOf (nHintData_1[nIndex][i]);
				String	key 	= nHintArray[a] + "," + nCount;
				nFlag 			= getUseNumberFlag (key);

				for(int j = 1 ; j <= nLoopEnd_1 ; j++){
					if(nDirection == (int)AREA.ROW){
						nIndex_plate = nPos+j;
					} else {
						nIndex_plate = nPos+(j*nRowMax);
					}
					
					//	数字が確定していないか
					if(plateData[nIndex_plate].fDefineNumber == false){
						
						int	nFlag_plate = plateData[nIndex_plate].nUseNumberFlag_op;
						if(nFlag_plate == 0x000000000){	
							nFlag_plate |= nFlag;
						} else {
							nFlag_plate &= nFlag;
						}
						int	nDefineNumberCount = getDefineNumberCount (nFlag_plate);
						//	入力可能な値が確定しているか
						if(nDefineNumberCount == 1){
							if(checkUseHintNumber (nPos, (int)nHintArray[a], nDirection) == false){
								
								nHintArray.RemoveAt (a);
								break;
								
							}
						} else if(nDefineNumberCount == 0){

							int	n = nHintArray.IndexOf (nHintArray[a]);
							if(n > -1)	nHintArray.RemoveAt (n);
							break;

						}

					}
					
				}
				
			}

			if(numberList.Count > 0){

				int nRand = UnityEngine.Random.Range (0, numberList.Count);
				for(int j = 1 ; j <= nLoopEnd_1 ; j++){		
					if(nDirection == (int)AREA.ROW){
						nIndex_plate = nPos+j;
					} else {
						nIndex_plate = nPos+(j*nRowMax);
					}

					String	key 	= (int)numberList[nRand] + "," + nCount;
					nFlag 			= getUseNumberFlag (key);
					if(plateData[nIndex_plate].nUseNumberFlag_op == 0x000000000){	
						plateData[nIndex_plate].nUseNumberFlag_op |= nFlag;
					} else {
						plateData[nIndex_plate].nUseNumberFlag_op &= nFlag;
					}
					
				}
				return	(int)nHintArray[nRand];

			} else {

				int		nRand 	= 0;
				bool	flag 	= false;
				do{

					nRand 	= UnityEngine.Random.Range (0, nHintData_1[nCount-2].Length);
					flag 	= checkUseHintNumber (nPos, nHintData_1[nIndex][nRand], nDirection);

				}while(flag == false);

				return	nHintData_1[nIndex][nRand];

			}

		}

	}

	/// <summary>
	/// 使用可能なフラグを返す
	/// </summary>
	int	getUseNumberFlag(String key){

		if(key == "")	return	0x000000000;

		int[]		nNum 	= new int[9];
		int 		nFlag 	= 0x000000000;

		for(int i = 8 ; i >= 0 ; i--){
			
			nNum[i] = (int)json_useNumber[key][i];
			if(nNum[i] == 1){
				
				int		nBuf = 0x000000001;
				nFlag |= nBuf<<(9-i)-1;
				
			}
			
		}
		
		return	nFlag;

	}

	int	getRowFlag(int nPos){

		int[]		nRowNum 	= new int[9];
		int 		nRowFlag 	= 0x000000000;
		String 		rowKey 		= plateData[nPos].nRowNumber + "," + plateData[nPos].nRowCount;

		for(int i = 8 ; i >= 0 ; i--){
			
			nRowNum[i] = (int)json_useNumber[rowKey][i];
			if(nRowNum[i] == 1){
				
				int		nBuf = 0x000000001;
				nRowFlag |= nBuf<<(9-i)-1;
				
			}
			
		}

		return	nRowFlag;

	}

	int	getColFlag(int nPos){

		int[]		nColNum 	= new int[9];
		int 		nColFlag 	= 0x000000000;
		String 		colKey 		= plateData[nPos].nColNumber + "," + plateData[nPos].nColCount;
			
		for(int i = 8 ; i >= 0 ; i--){
			
			nColNum[i] = (int)json_useNumber[colKey][i];
			if(nColNum[i] == 1){
				
				int		nBuf = 0x000000001;
				nColFlag |= nBuf<<(9-i)-1;
				
			}
			
		}
		
		return	nColFlag;

	}

	bool	checkArea(int nHintPos, int nCheckPos, int nDirection){

		int 	nStart 	= 0;	//	ループ最小値
		int 	nEnd 	= 0;	//	ループ最大値

		if(plateData[nHintPos].nPlateType != (int)PLATE_TYPE.HINT){

			return	false;

		}

		if(nDirection == (int)AREA.ROW){
			if(plateData[nHintPos].nRowCount == 0){
				
				return	false;
				
			}

			int 	nCheckRow 		= nCheckPos % nRowMax;	
			int 	nHintRow 		= nHintPos % nRowMax;	

			nStart 	= nHintRow + 1;
			nEnd 	= (nStart + plateData[nHintPos].nRowCount) - 1;
			
			if(nStart <= nCheckRow && nEnd >= nCheckRow){
				
				return	false;
				
			} else {

				return	false;

			}
	
		} else if(nDirection == (int)AREA.COL){
			if(plateData[nHintPos].nColCount == 0){

				return	false;

			}

			int 	nCheckCol 		= nCheckPos / nColMax;
			int 	nHintCol 		= nHintPos / nColMax;

			nStart 	= nHintCol + 1;
			nEnd 	= (nStart + plateData[nHintPos].nColCount) - 1;

			if(nStart <= nCheckCol && nEnd >= nCheckCol){
	
				return	true;
				
			} else {

				return	false;

			}
		
		}

		return	true;

	}

*/
	
}