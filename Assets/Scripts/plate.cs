﻿using UnityEngine;
using System.Collections;

public class plate : MonoBehaviour {
	
	public PLATEDATA	plateData;
	public GameObject	framePrefab;
	public GameObject	btn_deletePrefab;

	// Use this for initialization
	void Start () {
	}

	public void	Init(){

		plateData.nRowNumber 		= 0;
		plateData.nColNumber 		= 0;
		plateData.nNumber_Answer 	= 0;
		plateData.nNumber_Input 	= 0;
		plateData.nUseNumberFlag 	= 0x000000000;
		plateData.nUseNumberFlag_op = 0x000000000;
		plateData.fInput 			= false;
		plateData.fSetFlag 			= false;
		plateData.fDefineNumber		= false;

		if(plateData.nPlateType == (int)PLATE_TYPE.INPUT){
	
			transform.FindChild("number").GetComponent<UISprite>().spriteName = "info_num_00";
			transform.FindChild("number").gameObject.SetActive (false);

		}

		if(plateData.nPlateType == (int)PLATE_TYPE.HINT){
			if(plateData.nRowCount > 0){
				for(int i = 0 ; i < 2 ; i++){

					transform.FindChild("hintNumber_row_0"+i.ToString ()).GetComponent<UISprite>().spriteName = "info_num_00w";
					transform.FindChild("hintNumber_row_0"+i.ToString ()).gameObject.SetActive (false);

				}
			}

			if(plateData.nColCount > 0){
				for(int i = 0 ; i < 2 ; i++){
					
					transform.FindChild("hintNumber_col_0"+i.ToString ()).GetComponent<UISprite>().spriteName = "info_num_00w";
					transform.FindChild("hintNumber_col_0"+i.ToString ()).gameObject.SetActive (false);

				}
			}
		}

	}

	public	void	createFrame(){

		GameObject	frameData 							= Instantiate (framePrefab, Vector3.zero, Quaternion.identity) as GameObject;
		frameData.transform.parent 						= transform;
		frameData.transform.localScale 					= new Vector3 (1.0f, 1.0f, 1.0f);
		frameData.transform.localPosition 				= new Vector3 (-5.0f, 4.0f, 0.0f);
		frameData.GetComponent<UISprite> ().depth 		= 6;
		frameData.name 									= "frame";
		frameData.GetComponent<UISprite> ().spriteName 	= "";

	}

	public void	createDeleteButton(){

		GameObject btn_deleteBtnData 					= Instantiate (btn_deletePrefab, Vector3.zero, Quaternion.identity) as GameObject;
		btn_deleteBtnData.transform.parent 				= transform;
		btn_deleteBtnData.transform.localScale 			= new Vector3 (1.0f, 1.0f, 1.0f);
		btn_deleteBtnData.transform.localPosition 		= new Vector3 (25.0f, 25.0f, -1.0f);
		btn_deleteBtnData.transform.FindChild ("Background").GetComponent<UISprite>().depth = 20;
		btn_deleteBtnData.name 							= "btn_delete";
		btn_deleteBtnData.gameObject.SetActive (false);

	}
		
	// Update is called once per frame
	void Update () {

		if(plateData.fTouch == true){
//			Color	color = new Color((227.0f/255.0f), (162.0f/255.0f), (36.0f/255.0f), (255.0f/255.0f));
//			transform.FindChild ("Background").GetComponent<UISprite>().color = color;
		} else {
			transform.FindChild ("Background").GetComponent<UISprite>().color = Color.white;
		}

		if(plateData.nPlateType == (int)PLATE_TYPE.INPUT){
			if(plateData.fInput == true){
				if(GameObject.Find ("btn_debug").GetComponent<debug>().flag == false){

					transform.FindChild("number").GetComponent<UISprite>().spriteName = "info_num_0"+plateData.nNumber_Input.ToString ();
					transform.FindChild("number").gameObject.SetActive (true);

				}

			} else {

		//		transform.FindChild("number").GetComponent<UISprite>().spriteName = "info_num_01";
		//		transform.FindChild("number").gameObject.SetActive (false);

			}
		}
	
	}
	
}
